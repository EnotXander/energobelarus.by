<?php
header("Content-Type: text/xml");
print "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n";?>

<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:wfw="http://wellformedweb.org/CommentAPI/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
>

   <channel>
      <title>������� EnergoBelarus.by</title>
      <link>http://stats.energobelarus.by/</link>
      <description>������� ������ - EnergoBelarus.by</description>
      <pubDate><? print date("r"); ?></pubDate>
      <language>ru</language>

      <?include "shared.php";

      $e = explode(":", $_SERVER["QUERY_STRING"]);
      $totalcount = intval($e[0]);
      if ($totalcount == 0)
         $totalcount = 5;

      $rubric = intval($e[1]);

      $url = Array();
      $url = "http://" . $_SERVER["HTTP_HOST"] . "/c/cshow";

      if ($rubric != 0)
         $url.="/c/cshow?s:o:0:" . $rubric;

      $d = file($url);
      $num = 0;
      for ($i = 0; $i < count($d); $i++)
      {
         $e = explode("<!-- s -->", $d[$i]);
         if (count($e) == 9)
         {
            $description = "";
            $description.="������: " . strip_tags($e[3]) . " <small>" . strip_tags($e[2]) . "</small><br/> ";
            $description.="�������������: " . strip_tags($e[5]) . " <small>" . strip_tags($e[4]) . "</small><br/> ";
            $description.="�����: " . strip_tags($e[7]) . " <small>" . strip_tags($e[6]) . "</small><br/> ";

            if (preg_match("/^<a target=\"_blank\" href=\"(.+)\">(.*)<\/a>$/", $e[1], $r))
            {
               $url = $r[1];
               $title = htmlspecialchars($r[2]);
            }
            ?>
            <item>
               <title><?php print $title . " - " . intval(strip_tags($e[5])); ?></title>
               <link><?php print $url; ?></link>
               <pubDate><?php print date("r"); ?></pubDate>
               <guid isPermaLink="false"><?php print $url; ?></guid>
               <description><![CDATA[<?php print strip_tags($description); ?>]]></description>
               <content:encoded><![CDATA[<?php print $description; ?>]]></content:encoded>
            </item>
            <?
         }
      }
      ?>
   </channel>
</rss>