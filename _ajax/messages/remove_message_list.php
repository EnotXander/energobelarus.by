<? if(!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// ����� ��� ������ � pusher
//require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/pusher.php');

if (!CModule::IncludeModule("socialnetwork"))
{
    ShowError(GetMessage("SONET_MODULE_NOT_INSTALL"));
    return;
}

global $USER;

$partnerID = (int) $_REQUEST["PARTNER_ID"];

$messageID = (int) $_REQUEST["MESSAGE_ID"];

if($USER->IsAuthorized() && $partnerID > 0)
{
    $strToSQL =     "SELECT `ID`
                    FROM `b_sonet_messages` 
                    WHERE (`FROM_USER_ID` = {$DB->ForSql($USER->GetID())} AND `TO_USER_ID` = {$DB->ForSql($partnerID)} AND `FROM_DELETED` = 'N')  OR (`TO_USER_ID` = {$DB->ForSql($USER->GetID())} AND `FROM_USER_ID` = {$DB->ForSql($partnerID)} AND `TO_DELETED` = 'N')
                    ORDER BY `ID` DESC";

    $rsMes = $DB->Query($strToSQL, false, $err_mess.__LINE__);
        
    $messagesIDs = array();
    while($mess = $rsMes->Fetch())
    {
        $messagesIDs[] = $mess["ID"];
    }
    
    if (count($messagesIDs) > 0)
    {
        //$pusher = new Pusher(PUSHER_API_KEY, PUSHER_SECRET_KEY, PUSHER_APP_KEY);
        //$pusher->trigger("messages_channel".$USER->GetID(), "remove_message", array("messageId" => $messagesIDs));
        CSocNetMessages::DeleteMessageMultiple($USER->GetID(),$messagesIDs);
        echo json_encode(array("ERROR" => false, "MSGs" => $messagesIDs));
    } else
        echo json_encode(array("ERROR" => true));
} else if($USER->IsAuthorized() && $messageID > 0) // �������� ���������� ���������
{
    if(CSocNetMessages::DeleteMessage($messageID, $USER->GetID()))
        echo json_encode(array("ERROR" => false));
    else
        echo json_encode(array("ERROR" => true));
} else
    echo json_encode(array("ERROR" => true));