<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// ����� ��� ������ � pusher
require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/pusher.php');


if (!CModule::IncludeModule("socialnetwork"))
{
   ShowError(GetMessage("SONET_MODULE_NOT_INSTALL"));
   return;
}

global $APPLICATION, $USER, $DB;

$jsonPartners = $_REQUEST["PARTNERS_ID"];
$message = trim($_REQUEST["MSG"]);
$arPartners = json_decode($jsonPartners);
$isError = false;

$message = iconv("UTF-8", "cp1251", $message);

foreach ($arPartners as $partnerID)
{
   if ($partnerID > 0 && strlen($message) > 0)
   {
      if ($messageID = CSocNetMessages::Add(array("FROM_USER_ID" => $USER->GetID(), "TO_USER_ID" => $partnerID, "MESSAGE" => $message, "MESSAGE_TYPE" => "P", "=DATE_CREATE" => $DB->CurrentTimeFunction())))
      {
         $messageInfo = CSocNetMessages::GetByID($messageID);
         $messageInfo["MESSAGE_STRING"] = preg_replace("#<br[^>]*>#i", " ", $messageInfo["MESSAGE"]);
         $messageInfo["MESSAGE_STRING"] = str_replace(array('&nbsp;'), array(''), $messageInfo["MESSAGE_STRING"]);
         $messageInfo["MESSAGE_TITLE"] = str_replace(array('"', '<br />', '&nbsp;'), array('&quot;', ' ', ''), $messageInfo["MESSAGE"]);

         $result = $USER->GetList(($by = "personal_country"), ($order = "desc"), array("ID" => $USER->GetID()), array("SELECT" => array("ID", "PERSONAL_PHOTO", "NAME", "LAST_NAME", "UF_NICK_NAME")));
         $userInfo = $result->Fetch();
         if ($userInfo["PERSONAL_PHOTO"] > 0)
            $userInfo["PERSONAL_PHOTO"] = CFile::GetFileArray($userInfo["PERSONAL_PHOTO"]);

         if (strlen($userInfo["NAME"]) || strlen($userInfo["LAST_NAME"]))
         {
            $userInfo["FORMATED_NAME"] = $userInfo["NAME"];
            if (strlen($userInfo["FORMATED_NAME"]))
               $userInfo["FORMATED_NAME"] .=" ";
            $userInfo["FORMATED_NAME"] .= $userInfo["LAST_NAME"];
         }
         else
            $userInfo["FORMATED_NAME"] = $userInfo["LOGIN"];

         //������ ���������� � ���������
         $data = array("FROM" => array("ID" => $userInfo["ID"],
                 "FORMATED_NAME" => $userInfo["FORMATED_NAME"],
                 "PERSONAL_PHOTO" => $userInfo["PERSONAL_PHOTO"]["SRC"]),
             "TO" => array("ID" => $partnerID),
             "MESSAGE" => array("ID" => $messageID,
                 "TEXT" => $messageInfo["MESSAGE"],
                 "DATE" => $messageInfo["DATE_CREATE"]));
         //
         ob_start();
         $APPLICATION->IncludeComponent("whipstudio:socialnetwork.messages_chat", "ajax", array(
                  "USER_ID" => $partnerID,
                  "FROM" => $messageID-1,
                  "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
                  "SET_TITLE" => "Y"
              )
          );
         $chatHTML = ob_get_clean();
         $chatHTML = json_decode($chatHTML);
         // ���������� ����������� � ����� ���������
         $pusher = new Pusher(PUSHER_API_KEY, PUSHER_SECRET_KEY, PUSHER_APP_KEY);
         $pusher->trigger("messages_channel{$userInfo["ID"]}", "new_message", false);
         $pusher->trigger("messages_channel{$partnerID}", "new_message", false);
         $pusher->trigger("messages_channel{$partnerID}", "incoming_message", false);
      } else
      {
         if ($e = $GLOBALS["APPLICATION"]->GetException())
            $errorMessage .= $e->GetString();
         $isError = true;
         break;
      }
   }
   else $isError = true;
}

echo json_encode(array("ERROR" => $isError, "LAST" => $messageID, "PARTNER_ID" => $arPartners, "CHAT" => $chatHTML));