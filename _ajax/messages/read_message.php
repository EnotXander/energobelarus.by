<? if(!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// ����� ��� ������ � pusher
if(!CModule::IncludeModule("socialnetwork")) die;
require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/pusher.php');
$pusher = new Pusher(PUSHER_API_KEY, PUSHER_SECRET_KEY, PUSHER_APP_KEY);

$jsonErr = 1;
global $USER;

$messagesIDs = explode(',', $_POST['MESSAGE_IDs']);
$messageIDs = array();
$messageMeta = array();
if(is_array($messagesIDs))
{    
    $jsonErr = 0;
    foreach($messagesIDs as $messageId)
    {
        $messageId = (int)trim($messageId);
        if($messageId > 0)
        {
            if(CSocNetMessages::MarkMessageRead($USER->GetID(),$messageId)) 
            {     
                $messageIDs[] = $messageId;
                $arMessage = CSocNetMessages::GetByID($messageId);
                $messageMeta[$arMessage["FROM_USER_ID"]][] = $messageId;
            }     
        }
    }
}

if(count($messageIDs))
{
   $pusher->trigger("messages_channel".$USER->GetID(), "read_message", array("messageId" => array($messageId)));
   foreach($messageMeta as $userId => $arMessages)
   {
      $pusher->trigger("messages_channel".$userId, "read_message", array("messageId" => $arMessages));
   }
}

echo json_encode(array('err' => $jsonErr, 'messageId' => $messageIDs));
