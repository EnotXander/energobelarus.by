<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");

$companyId = (int)$_POST["companyId"];
$status = 'FAIL';

if ($companyId)
{
    if(!CModule::IncludeModule("iblock")) return;
    $rsCompany = CIBlockElement::GetList(
        array("SORT" => "ASC", "NAME" => "ASC"),
        array(
            "IBLOCK_ID" => 17,
            "ID" => $companyId
        ),
        false,
        false,
        array('ID', 'IBLOCK_ID', 'PROPERTY_PHONE_CLICK_COUNT')
    );
    if($arCompany = $rsCompany->GetNext())
    {
        $counter = ($arCompany['PROPERTY_PHONE_CLICK_COUNT_VALUE']) ? $arCompany['PROPERTY_PHONE_CLICK_COUNT_VALUE'] : 0;
        $counter++;
        CIBlockElement::SetPropertyValues(
            $companyId,
            17,
            $counter,
            "PHONE_CLICK_COUNT"
        );

        $status = 'OK';
    }
}

echo json_encode(array('STATUS' => $status));
