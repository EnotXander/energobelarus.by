<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $USER;
if(!$USER->IsAuthorized()) LocalRedirect("/");


$APPLICATION->IncludeComponent("whipstudio:photos.add", "", array(
    'IBLOCK_ID' => 10,
    'SCRIPT_INCLUDE' => "http://".SITE_SERVER_NAME.SITE_TEMPLATE_PATH."/js/jquery.fileupload.js",
    'AJAX_PATH' => '/_ajax/photos/upload.php',
));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>