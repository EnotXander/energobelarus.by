<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use \Glifery\Crm\Logger;
$logger = new Logger();

global $DB;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();

//�������� �������� � ��������
$res = $objElement->GetList(
        array("ID" => "DESC"),
        array("IBLOCK_ID" => 17),
        false,
        false,
        array("ID", "PROPERTY_TARIF", "PROPERTY_TARIF_DATE_START", "PROPERTY_TARIF_DATE_STOP", "PROPERTY_MANAGER")
);
$arCompanyBX = array();
while ($arItem = $res->GetNext()) {
   $arCompanyBX[$arItem["ID"]] = $arItem;
}

//�������� �������� �� CRM
$arCompanyCRM = array();
$query = "SELECT * FROM  `a_crm_out` order by `COMPANY_ID` desc";
$res = $DB->Query($query);
while($arQuery = $res->fetch())
{
   $arCompanyCRM[$arQuery["COMPANY_ID"]] = $arQuery;
}

//������ ���������� � ��������
$arStatus = array();
foreach($arCompanyBX as $companyBXId => $companyBX)
{
   $status = array("ID" => $companyBXId);
   if(isset($arCompanyCRM[$companyBXId]))
   {
      $status["FOUND"] = true;
      $companyCRM = $arCompanyCRM[$companyBXId];
      $arSaveProps = array();
      //predstavitel
      if($companyBX["PROPERTY_MANAGER_VALUE"] != $companyCRM["MANAGER_ID"])
      {
         //$status["PREDSTAVITEL"] = PredstavitelSetStatus($companyCRM["COMPANY_ID"], $companyCRM["MANAGER_ID"], 65);
         $arSaveProps["MANAGER"] = $companyCRM["MANAGER_ID"];
         if(!$arSaveProps["MANAGER"]) $arSaveProps["MANAGER"] = false;
      }
      //tarif
      if($companyBX["PROPERTY_TARIF_VALUE"] != $companyCRM["TP_ID"])
      {
         $arSaveProps["TARIF"] = $companyCRM["TP_ID"];
         if(!$arSaveProps["TARIF"]) $arSaveProps["TARIF"] = false;
      }
      //date_start
      if($companyCRM["DT_START"] !== null)
      {
         if(BXToTimestamp($companyBX["PROPERTY_TARIF_DATE_START_VALUE"]) != DBToTimestamp($companyCRM["DT_START"]))
            $arSaveProps["TARIF_DATE_START"] = DBToBX($companyCRM["DT_START"]);
      }
      else
      {
         if($companyBX["PROPERTY_TARIF_DATE_START_VALUE"] !== null)
            $arSaveProps["TARIF_DATE_START"] = "";
      }
      //date_stop
      if($companyCRM["DT_STOP"] !== null)
      {
         if(BXToTimestamp($companyBX["PROPERTY_TARIF_DATE_STOP_VALUE"]) != DBToTimestamp($companyCRM["DT_STOP"]))
            $arSaveProps["TARIF_DATE_STOP"] = DBToBX($companyCRM["DT_STOP"]);
      }
      else
      {
         if($companyBX["PROPERTY_TARIF_DATE_STOP_VALUE"] !== null)
            $arSaveProps["TARIF_DATE_STOP"] = "";
      }
      //����������
      if(count($arSaveProps))
      {
          $message = Logger::createMessage('out');
          $message->setCompanyId($companyBXId);
          foreach($arSaveProps as $field => $value) {
            $message->addUpdatedField($field, $value);
          }
          $logger->log($message);

         $status["UPDATED"] = true;
         $status["FIELDS"] = $arSaveProps;
         CIBlockElement::SetPropertyValuesEx($companyBXId, 17, $arSaveProps);
      }
      else
         $status["UPDATED"] = false;
   }
   else
      $status["FOUND"] = false;
   $arStatus[$companyBXId] = $status;
}
//PrintAdmin($arStatus);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>