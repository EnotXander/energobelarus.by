<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?> 
<div class="rc_block brdr_box"> 

<?
die();
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/classes/general/csv_data.php");
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();

$arFilter = array(
   "IBLOCK_ID" => 17
);
$resNew = $objElement->GetList(array(), $arFilter);
while($obj = $resNew->GetNextElement())
{
   $company = $obj->GetFields();
   $company["PROP"] = $obj->GetProperties();
   
   if(count($company["PROP"]["URL"]["VALUE"]) == 1)
   {
      $newVal = array();
      foreach($company["PROP"]["URL"]["VALUE"] as $value)
      {
         $temp = explode(" ", trim($value));
         foreach($temp as $site)
         {
            $newVal[] = trim($site);
         }
      }
      if(count($newVal) > 1)
         CIBlockElement::SetPropertyValues($company["ID"], 17, $newVal, "URL");
   }   
}
?>
   
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>