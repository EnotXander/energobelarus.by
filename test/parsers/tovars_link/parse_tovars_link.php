<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 200,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
    "IBLOCK_ID" => 27//17-company
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_TOVARS_LINK_ALL'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $arCurrent = current($arSessionId);
      $currentName = $arCurrent[0];
      //$currentName = "name";//*******
      $currentFirm = $arCurrent[1];
      $res = $objElement->GetList(
               array("ID" => "DESC"),
               array("IBLOCK_ID" => 27, "NAME" => $currentName),
               false
          );
      $arItems = array();
      while($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROPERTIES"] = $obItem->GetProperty("FIRM");
         $arItems[] = $arItem;
      }
       
      //$res = $objElement->GetByID($currentId);
      //if($obItem = $res->GetNextElement())
      if(count($arItems) > 1)
      {
         //���� ������� ��������� ������� �� ��������
         foreach($arItems as $key => $item)
         {
            if(!$item["PROPERTIES"]["VALUE"])
            {
               $arItems = array($item);
               break;
            }
         }
      }
      if(count($arItems))
      {
         foreach ($arItems as $arItem)
         {
            //$arItem = $arItems[0];
            if(!$arItem["PROPERTIES"]["VALUE"])//���� �������� �� ������
            {
               $resFirm = $objElement->GetByID($currentFirm);
               if($obFirm = $resFirm->GetNextElement())
               {
                  $arFirm = $obFirm->GetFields();
                  $arFirm["PROPERTIES"] = $obFirm->GetProperty("REMOVE_REL");
                  //�������� ������� ��������
                  if(!$arFirm["PROPERTIES"]["VALUE"])//���� ������� �� ������
                  {
                     $arChange = $_SESSION['PARSE_TOVARS_LINK_UNRELATED'];
                     $arChange[] = array(
                        "ID" => $arItem["ID"],
                        "NAME" => $arItem["NAME"],
                        "FIRM" => $arFirm["ID"],
                        "FIRM_NAME" => $arFirm["NAME"],
                        "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"],
                        "LINK_FIRM" => "http://".SITE_SERVER_NAME.$arFirm["DETAIL_PAGE_URL"],
                     );
                     $_SESSION['PARSE_TOVARS_LINK_UNRELATED'] = $arChange;

                     if($KEY["SAVE"])
                     {
                        $objElement->SetPropertyValues($arFirm["ID"], 17, 63, "REMOVE_REL");
                        if($KEY["PRINT"])
                           PrintObject ("���������! {$arFirm["ID"]} ({$arFirm["NAME"]}) - {$arUsers[$arItem["ID"]]["ID"]} ({$arUsers[$arItem["ID"]]["NAME"]} {$arUsers[$arItem["ID"]]["LAST_NAME"]})");
                     }
                  }
                  else
                  {
                     //���� ������� ������
                     $arChange = $_SESSION['PARSE_TOVARS_LINK_ALREADY_UNRELATED'];
                     $arChange[] = array(
                        "ID" => $arItem["ID"],
                        "NAME" => $arItem["NAME"],
                        "FIRM" => $arFirm["ID"],
                        "FIRM_NAME" => $arFirm["NAME"],
                        "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"],
                        "LINK_FIRM" => "http://".SITE_SERVER_NAME.$arFirm["DETAIL_PAGE_URL"],
                        "RELATION" => $arFirm["PROPERTIES"]["VALUE"]
                     );
                     $_SESSION['PARSE_TOVARS_LINK_ALREADY_UNRELATED'] = $arChange;
                  }

                  //�������� �������� ������
                  $arChange = $_SESSION['PARSE_TOVARS_LINK_SAVE'];
                  $arChange[] = array(
                     "ID" => $arItem["ID"],
                     "NAME" => $arItem["NAME"],
                     "FIRM" => $arFirm["ID"],
                     "FIRM_NAME" => $arFirm["NAME"],
                     "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"],
                     "LINK_FIRM" => "http://".SITE_SERVER_NAME.$arFirm["DETAIL_PAGE_URL"],
                     "RELATION" => $arFirm["PROPERTIES"]["VALUE"]
                  );
                  $_SESSION['PARSE_TOVARS_LINK_SAVE'] = $arChange;
                  if($KEY["SAVE"])
                  {
                     $objElement->SetPropertyValues($arItem["ID"], 27, $arFirm["ID"], "FIRM");
                     if($KEY["PRINT"])
                        PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) - {$arUsers[$arItem["ID"]]["ID"]} ({$arUsers[$arItem["ID"]]["NAME"]} {$arUsers[$arItem["ID"]]["LAST_NAME"]})");
                  }
               }
            }
            else
            {
               //�������� ��� ������
               $arChange = $_SESSION['PARSE_TOVARS_LINK_ALREADY'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "FIRM" => $arItem["PROPERTIES"]["VALUE"],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_TOVARS_LINK_ALREADY'] = $arChange;
            }
         }
      }
      /*elseif(count($arItems) > 1)
      {
         //���� ������� ��������� ������� �� ��������
         foreach($arItems as $key => $item)
         {
            $arItems[$key] = $item["ID"];
         }
         $arChange = $_SESSION['PARSE_TOVARS_LINK_MULTI'];
         $arChange[] = array(
            "IDs" => $arItems,
            "NAME" => $currentName,
         );
         $_SESSION['PARSE_TOVARS_LINK_MULTI'] = $arChange;
      }*/
      else
      {
         //���� �� ������� �� ������ ������ �� ��������
         $arChange = $_SESSION['PARSE_TOVARS_LINK_NOTFOUND'];
         $arChange[] = array(
            "NOT_FOUND" => $arCurrent
         );
         $_SESSION['PARSE_TOVARS_LINK_NOTFOUND'] = $arChange;
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($arCurrent, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_TOVARS_LINK_ALL'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_tovars_link.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}