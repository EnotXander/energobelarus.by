<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
if(!CModule::IncludeModule("iblock")) die();

die();

$KEY = array(
    "SAVE_IN_SESSION" => false,
    "PARSE_AND_CHANGE" => true,
    "SAVE" => false,
    "MAXCOUNT" => 300,
    "PRINT" => true,
    "IBLOCK_ID" => 33
);

//PrintObject($_SESSION['PARSE_IMG_IDS']);

$objElement = new CIBlockElement();
session_start();

if($KEY["SAVE_IN_SESSION"])
{
   $arSessionId = array();
   $res = $objElement->GetList(
           array("ID" => "ASC"),
           array("IBLOCK_ID" => $KEY["IBLOCK_ID"], "ACTIVE" => "Y"),
           false
      );
   while($arItem = $res->GetNext())
   {
      $arSessionId[] = $arItem["ID"];
   }
   $_SESSION['PARSE_IMG_IDS'] = $arSessionId;
   if($KEY["PRINT"])
   {
      PrintObject("SAVE_IN_SESSION");
      PrintObject($_SESSION['PARSE_IMG_IDS']);
   }
   
}

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_IMG_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         //��������� ������ ����������� ��������
         $arPictureId = array();
         if($arItem["PREVIEW_PICTURE"])
            $arPictureId[] = $arItem["PREVIEW_PICTURE"];
         if($arItem["DETAIL_PICTURE"])
            $arPictureId[] = $arItem["DETAIL_PICTURE"];

         //�������� �������� ��������
         $arOriginalName = array();
         foreach($arPictureId as $pictureId)
         {
            $arPictureFile = CFile::GetFileArray($pictureId);
            $arOriginalName[] = $arPictureFile["ORIGINAL_NAME"];
            //PrintObject($arPictureFile);
         }

         //��������� ������ �����, ������� ���������� ��������
         $arParseFields = array();
         $arParseFields[] = "PREVIEW_TEXT";
         $arParseFields[] = "DETAIL_TEXT";
         $arParseFieldsChange = array();

         //������ ���� � �������� ����
         foreach($arParseFields as $parseField)
         {
            $parseText = $arItem[$parseField];
            //PrintObject($parseText);
            if(strlen($parseText))
            {
               foreach($arOriginalName as $parseName)
               {
                  $reg = "/(\<img[^>]*src=[\"\']?[^\"\'\s]*[.\/[^\"\'\s]]?{$parseName}[\"\']?[^>]*\>)/i";
                  $newText = preg_replace($reg, "", $parseText);
                  if($newText !== NULL)
                  {
                     if($parseText != $newText)
                     {
                        $arParseFieldsChange[$parseField] = $newText;
                        if($KEY["PRINT"])
                        {
                           PrintObject(" ");
                           PrintObject(" ");
                           PrintObject("( ".$arItem["ID"]." )  ".$arItem["NAME"]."  ( ".$parseName." )");
                        }
                     }
                  }
               }
            }
         }
         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]}");
               }
            }
         }
      }
      //�������� �� ������
      $currentKey = array_search($currentId, $arSessionId);
      if($currentKey !== false)
      {
         unset($arSessionId[$currentKey]);
         $_SESSION['PARSE_IMG_IDS'] = $arSessionId;
      }
   }
   //�������������
   if(count($arSessionId) && !$KEY["MAXCOUNT"])
   {
      LocalRedirect("text.php");
      PrintObject("temp end");
   }
   else
   {
      PrintObject("END!!!");
   }
}


?>