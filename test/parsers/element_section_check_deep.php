<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//die();

if(!CModule::IncludeModule("iblock")) die();
$objSection = new CIBlockSection();
$objElement = new CIBlockElement();
/*$isUpdated = $objElement->Update(55730);
if(!$isUpdated)
   PrintAdmin ($objElement->LAST_ERROR);
else
   PrintAdmin ("UPD!!!");*/

$KEY = array(
    "IBLOCK_ID" => 27//3-news 2-articles 29-interviews 27-tovars 17-company 33-faces
);


$arSectionId = array();
$res = $objSection->GetList(
        array("ID" => "DESC"),
        array("IBLOCK_ID" => $KEY["IBLOCK_ID"]),
        false
   );
while($arItem = $res->GetNext())
{
   $arSectionId[] = (int)$arItem["ID"];
}

foreach ($arSectionId as $section)
{
   $res = $objElement->GetList(
           array("ID" => "DESC"),
           array("IBLOCK_ID" => $KEY["IBLOCK_ID"], "SECTION_ID" => $section),
           false,
           false,
           array("ID", "NAME", "ACTIVE_FROM", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "IBLOCK_SECTION_ID.NAME")
      );
   while($arItem = $res->GetNext())
   {
      if($arItem["IBLOCK_SECTION_ID"] != $section)
      {
         $arGroups = array();
         $rsGroups = $objElement->GetElementGroups($arItem["ID"]);
         while($group = $rsGroups->GetNext())
            $arGroups[] = $group["ID"];
         if/*($arItem["IBLOCK_SECTION_ID"] != $section)*/(!in_array($section, $arGroups))
         {
            $log["FIELD"][$arItem["IBLOCK_SECTION_ID"]]++;
            $log["GTLST"][$section]++;
            //if($isUpdated = $objElement->Update(55730, array("IBLOCK_SECTION_ID" => )))
            $arElementId[] = array(
                "ID" => $arItem["ID"],
                "NAME" => $arItem["NAME"],
                "ACTIVE_FROM" => $arItem["ACTIVE_FROM"],
                "DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"],
                "GROUP___SECTIONS" => $arGroups,
                "GET_LIST_SECTION" => $section
            );
         }
      }
   }
}

PrintAdmin($log);
PrintAdmin($arElementId);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");