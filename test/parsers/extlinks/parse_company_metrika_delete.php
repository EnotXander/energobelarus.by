<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 100,//100 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
    "IBLOCK_ID" => 17
);

$yaMetrika = YandexMetrika::getInstance(array(
   "ID" => METRIKA_CLIENT_ID,
   "SECRET" => METRIKA_CLIENT_SECRET,
   "USERNAME" => METRIKA_USERNAME,
   "PASSWORD" => METRIKA_PASSWORD
));

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 149691;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROP"] = $obItem->GetProperties();
         if($arItem["PROP"]["METRIKA_ID"]["VALUE"])
         {
            $yaMetrika->delete("/counter/{$arItem["PROP"]["METRIKA_ID"]["VALUE"]}");
            if(!strlen($yaMetrika->error))
            {
               if($KEY["SAVE"])
               {
                  $objElement->SetPropertyValues($arItem["ID"], $KEY["IBLOCK_ID"], false, "METRIKA_ID");
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) - {$arItem["PROP"]["METRIKA_ID"]["VALUE"]} ({$arItem["CODE"]})");
               }
               $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "METRIKA_ID" => $arItem["PROP"]["METRIKA_ID"]["VALUE"],
                  "CODE" => $arItem["CODE"],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
            }
            else
            {
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "METRIKA_ID" => $arItem["PROP"]["METRIKA_ID"]["VALUE"],
                  "ERROR" => $yaMetrika->error,
                  "CODE" => $arItem["CODE"],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_metrika_delete.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}