<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => false,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
    "IBLOCK_ID" => 17//17-company
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   //�������������� ������ ������
   $arUsers = array();
   $userFilter = Array (
      "!UF_COMPANY" => false,
      "GROUPS_ID" => Array(12)
   );
   $rsUsers = CUser::GetList(($by = "id"), ($order = "desc"), $userFilter, array("SELECT" => array("UF_COMPANY"))); // �������� �������������
   while($arUser = $rsUsers->GetNext()) 
   {
      if($arUser["UF_COMPANY"])
         $arUsers[$arUser["UF_COMPANY"]] = $arUser; 
   }
   //PrintObject($arUsers);

   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 28530;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROPERTIES"] = $obItem->GetProperty("USER");
         //PrintObject($arItem);
         if(!$arItem["PROPERTIES"]["VALUE"])//���� ������������� �� �����
         {
            if(isset($arUsers[$arItem["ID"]]))
            {
               if($KEY["SAVE"])
               {
                  $objElement->SetPropertyValues($arItem["ID"], $KEY["IBLOCK_ID"], $arUsers[$arItem["ID"]]["ID"], "USER");
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) - {$arUsers[$arItem["ID"]]["ID"]} ({$arUsers[$arItem["ID"]]["NAME"]} {$arUsers[$arItem["ID"]]["LAST_NAME"]})");
               }
            }
            else
            {
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = $arItem["ID"];
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
               if($KEY["PRINT"])
                  PrintObject ("�� ������ �������� ��� {$arItem["ID"]} ({$arItem["NAME"]})");
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_user.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}