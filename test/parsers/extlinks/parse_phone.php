<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
    "IBLOCK_ID" => 17
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 99229;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROP"] = $obItem->GetProperties();
         
         //���� ���������� �� �������������
         if(!strlen($arItem["PROP"]["phone"]["VALUE"][1]))
         {
            $newString = $arItem["PROP"]["phone"]["VALUE"][0];

            //����������
            $filter = false;
            $newStringFiltered = preg_replace('/[^0-9\+\-\(\) ,;]+/i', '*', $newString);
            if($newString != $newStringFiltered)
            {
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "PHONE" => $arItem["PROP"]["phone"]["VALUE"][0],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
            }
            //����������
            $newString = explode(",", $newString);
            if(count($newString) == 1)
               $newString = explode(";", $newString[0]);
            foreach ($newString as $wordKey => $word)
            {
               //$word = htmlspecialchars_decode($word);
               $word = preg_replace('/[\s	]+/', ' ', $word);
               $newString[$wordKey] = trim($word);
            }
            //�������� �� ���������� ������������
            if(strlen($arItem["PROP"]["phone"]["VALUE"][0]) && (count($newString) > 1))
            {
               $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "PHONE" => $arItem["PROP"]["phone"]["VALUE"][0],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
            }

            //����������
            if($KEY["SAVE"])
            {
               if(strlen($arItem["PROP"]["phone"]["VALUE"][0]) && (count($newString) > 1))
               {
                  $objElement->SetPropertyValues($arItem["ID"], $KEY["IBLOCK_ID"], $newString, "phone");
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_phone.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}