<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
    "IBLOCK_ID" => 17
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 28943;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROP"] = $obItem->GetProperties();
         
         //���� ���������� �� �������������
         if(strlen($arItem["PROP"]["keywords"]["VALUE"]))
         {
            $newArray = array();
            $newArray = unserialize(html_entity_decode($arItem["PROP"]["keywords"]["VALUE"]));
            if(strlen($newArray["TEXT"]))
            {
               $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "HTML" => $arItem["PROP"]["keywords"]["VALUE"],
                  "STRING" => $newArray["TEXT"],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
               //����������
               if($KEY["SAVE"])
               {
                  $objElement->SetPropertyValues($arItem["ID"], $KEY["IBLOCK_ID"], $newArray["TEXT"], "keywords");
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) - {$newArray["TEXT"]}");
               }
            }
            else
            {
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "HTML" => $arItem["PROP"]["keywords"]["VALUE"],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_keywords_type.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}