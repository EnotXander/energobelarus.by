<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 3000,//700 reliz
    "PRINT" => false,
    "IBLOCK_ID" => 29
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 80384;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         //��������� ���������
         $reg1 = '/(<table[^>]*)( border[ ]?=[ ]?(\"[^>\"]+\"|\\\'[^>\\\']+\\\'|[^> ]+))(( [^>]*>)|>)/';
         $reg2 = '/(<table[^>]*)( width[ ]?=[ ]?(\"[^>\"]+\"|\\\'[^>\\\']+\\\'|[^> ]+))(( [^>]*>)|>)/';
         $reg3 = '/(<table[^>]* style[ ]?=[ ]?[\\\'\"][^\'\"]*)(width[ ]?:[ ]?[^\\\'\"; ]+[;]?)([^\\\'\">]*[\\\'\"][^>]*>)/';
         $reg31 = '/(<table[^>]* style[ ]?=[ ]?[\\\'\"][^\'\"]*)(border[ ]?:[ ]?[^\\\'\";]+[;]?)([^\\\'\">]*[\\\'\"][^>]*>)/';
         $reg32 = '/(<table[^>]* style[ ]?=[ ]?[\\\'\"][^\'\"]*)(border-top[ ]?:[ ]?[^\\\'\"; ]+[;]?)([^\\\'\">]*[\\\'\"][^>]*>)/';
         $reg33 = '/(<table[^>]* style[ ]?=[ ]?[\\\'\"][^\'\"]*)(border-bottom[ ]?:[ ]?[^\\\'\"; ]+[;]?)([^\\\'\">]*[\\\'\"][^>]*>)/';
         $reg34 = '/(<table[^>]* style[ ]?=[ ]?[\\\'\"][^\'\"]*)(border-left[ ]?:[ ]?[^\\\'\"; ]+[;]?)([^\\\'\">]*[\\\'\"][^>]*>)/';
         $reg35 = '/(<table[^>]* style[ ]?=[ ]?[\\\'\"][^\'\"]*)(border-right[ ]?:[ ]?[^\\\'\"; ]+[;]?)([^\\\'\">]*[\\\'\"][^>]*>)/';
         $reg4 = '#(<td[^>]*[^-]width[ ]?[=:][^>]*>)#is';
         
         //��������� ������ �����, ������� ���������� ��������
         $arParseFields = array();
         $arParseFields[] = "DETAIL_TEXT";
         $arParseFieldsChange = array();

         //������ ���� � �������� ����
         foreach($arParseFields as $parseField)
         {
            $parseText = $arItem[$parseField];
            if(strlen($parseText))
            {
               /*PrintObject($currentId);
               PrintObject(htmlentities($parseText));*/
               $newText = preg_replace($reg1, "$1$4", $parseText);
               $newText = preg_replace($reg2, "$1$4", $newText);
               $newText = preg_replace($reg3, "$1$3", $newText);
               $newText = preg_replace($reg31, "$1$3", $newText);
               $newText = preg_replace($reg32, "$1$3", $newText);
               $newText = preg_replace($reg33, "$1$3", $newText);
               $newText = preg_replace($reg34, "$1$3", $newText);
               $newText = preg_replace($reg35, "$1$3", $newText);
               $found = preg_match($reg4, $newText);
               /*PrintObject("-----------------------------------------------------");
               PrintObject(htmlentities($newText));
               PrintObject("-----------------------------------------------------");
               PrintObject("-----------------------------------------------------");*/
               if($newText !== NULL)
               {
                  if($parseText != $newText)
                  {
                     PrintObject($currentId);
                     PrintObject(htmlentities($parseText));
                     PrintObject("-----------------------------------------------------");
                     PrintObject(htmlentities($newText));
                     PrintObject("-----------------------------------------------------");
                     PrintObject("-----------------------------------------------------");
                     
                     $arParseFieldsChange[$parseField] = $newText;
                     $arParseFieldsChange[$parseField."_TYPE"] = "html";//$arItem[$parseField."_TYPE"];
                     if($KEY["PRINT"])
                     {
                        PrintObject(" ");
                        PrintObject(" ");
                        PrintObject("( ".$arItem["ID"]." )  ".$arItem["NAME"]."  ( ".$parseField." )");
                     }
                  }
               }
               if($found)
               {
                  /*PrintObject($currentId);
                  PrintObject(htmlentities($parseText));
                  PrintObject("-----------------------------------------------------found");*/
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  $arChange[] = $arItem["ID"];
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
               }
            }
         }
         
         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  $arChange[] = $arItem["ID"];
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
                  
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      $currentKey = array_search($currentId, $arSessionId);
      if($currentKey !== false)
      {
         unset($arSessionId[$currentKey]);
         $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
      }
   }
   //�������������
   if(count($arSessionId) && !$KEY["MAXCOUNT"])
   {
      LocalRedirect("parse_table.php");
      PrintObject("temp end");
   }
   else
   {
      PrintObject("END!!!");
   }
}