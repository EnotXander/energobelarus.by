<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => false,
    "MAXCOUNT" => 1500,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   //�������������� ������ ������
   $arUsers = array();
   $userFilter = Array (
      "!UF_COMPANY" => false,
      "GROUPS_ID" => Array(12)
   );
   $rsUsers = CUser::GetList(($by = "id"), ($order = "desc"), $userFilter, array("SELECT" => array("UF_COMPANY"))); // �������� �������������
   while($arUser = $rsUsers->GetNext()) 
   {
      if($arUser["UF_COMPANY"])
         $arUsers[$arUser["UF_COMPANY"]] = $arUser; 
   }
   //PrintObject($arUsers);

   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 18629;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         //��������� ���������
         $reg1 = ' /(<a [^>]*href[ ]?=[ ]?[\\\'\"]?http[s]?:\/\/([^\/\\\'\"]*\.)?z2.by(\/[^> \\\'\"]*)?[\\\'\" ]?[^>]*>(.)*<\/[ ]?a>)/isU';
         //��������� ������ �����, ������� ���������� ��������
         $arParseFields = array();
         $arParseFields[] = "PREVIEW_TEXT";
         $arParseFields[] = "DETAIL_TEXT";
         $arParseFieldsChange = array();
         //PrintObject($arParseFields);
         //������ ���� � �������� ����
         foreach($arParseFields as $parseField)
         {
            $parseText = $arItem[$parseField];
            if(strlen($parseText))
            {
               //PrintObject("*********");
               $newText = preg_replace($reg1, "", $parseText);
               if($newText !== NULL)
               {
                  if($parseText != $newText)
                  {
                     $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                     $arChange[] = $arItem["ID"];
                     $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;

                     PrintObject($currentId." ".$arItem["NAME"]);
                     PrintObject(htmlentities($parseText));
                     PrintObject("-----------------------------------------------------");
                     PrintObject(htmlentities($newText));
                     PrintObject("-----------------------------------------------------");
                     PrintObject("-----------------------------------------------------");
                     
                     $arParseFieldsChange[$parseField] = $newText;
                     $arParseFieldsChange[$parseField."_TYPE"] = "html";//$arItem[$parseField."_TYPE"];
                     if($KEY["PRINT"])
                     {
                        PrintObject(" ");
                        PrintObject(" ");
                        PrintObject("( ".$arItem["ID"]." )  ".$arItem["NAME"]."  ( ".$parseField." )");
                     }
                  }
                  //else PrintObject(htmlspecialchars ($reg1));
               }
            }
         }
         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_z2.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}