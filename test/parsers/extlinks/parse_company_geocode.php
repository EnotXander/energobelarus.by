<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 60,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
    "IBLOCK_ID" => 17
);

if($KEY["PARSE_AND_CHANGE"])
{
   //��������� ������ �����, �������� � �������
   $arCountry = array();
   $res = $objElement->GetList(array("ID" => "ASC"),array("IBLOCK_ID" => 26, "ACTIVE" => "Y"),false);
   while($arItem = $res->GetNext())
      $arCountry[$arItem["ID"]] = $arItem["NAME"];
   $arRegion = array();
   $res = $objElement->GetList(array("ID" => "ASC"),array("IBLOCK_ID" => 22, "ACTIVE" => "Y"),false);
   while($arItem = $res->GetNext())
      $arRegion[$arItem["ID"]] = $arItem["NAME"];
   $arCity = array();
   $res = $objElement->GetList(array("ID" => "ASC"),array("IBLOCK_ID" => 23, "ACTIVE" => "Y"),false);
   while($arItem = $res->GetNext())
      $arCity[$arItem["ID"]] = $arItem["NAME"];
   
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 28530;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROP"] = $obItem->GetProperties();

         //PrintObject($arItem["PROP"]);
         if(strlen($arItem["PROP"]["adress"]["VALUE"]))
         {
            if(!strlen($arItem["PROP"]["MAP"]["VALUE"]))
            {
               $newString = array();
               if($arItem["PROP"]["COUNTRY"]["VALUE"] && strlen($arCountry[$arItem["PROP"]["COUNTRY"]["VALUE"]]))
                  $newString[] = $arCountry[$arItem["PROP"]["COUNTRY"]["VALUE"]];
               if($arItem["PROP"]["CITY"]["VALUE"] && strlen($arCity[$arItem["PROP"]["CITY"]["VALUE"]]))
                  $newString[] = $arCity[$arItem["PROP"]["CITY"]["VALUE"]];
               else
               {
                  if($arItem["PROP"]["REGION"]["VALUE"] && strlen($arRegion[$arItem["PROP"]["REGION"]["VALUE"]]))
                     $newString[] = $arRegion[$arItem["PROP"]["REGION"]["VALUE"]];
               }
               $newString[] = $arItem["PROP"]["adress"]["VALUE"];
               $newString = implode(", ", $newString);
               $saveString = "";
               $saveString = GetInfoByAddress($newString);
               if(strlen($saveString))
               {
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  $arChange[] = array(
                      "ID" => $arItem["ID"],
                      "NAME" => $arItem["NAME"],
                      "ADRESS" => $newString,
                      "MAP" => $saveString,
                      "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                  );
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
                  //����������
                  if($KEY["SAVE"])
                  {
                     $objElement->SetPropertyValues($arItem["ID"], $KEY["IBLOCK_ID"], $saveString, "MAP");
                     if($KEY["PRINT"])
                        PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) - {$saveString}");
                  }
               }
               else
               {
                  //�������������� ��� �������
                  $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
                  $arChange[] = array(
                      "ID" => $arItem["ID"],
                      "NAME" => $arItem["NAME"],
                      "ADRESS" => $newString,
                      "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                  );
                  $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
               }
            }
            else
            {
               //�������������� ��� �������
               /*$arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = array(
                   "ID" => $arItem["ID"],
                   "NAME" => $arItem["NAME"],
                   "ADRESS" => $arItem["PROP"]["adress"]["VALUE"],
                   "MAP" => $arItem["PROP"]["MAP"]["VALUE"],
                   "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;*/
            }
         }
         else
         {
            //����� �� ������
            /*$arChange = $_SESSION['PARSE_PREVPIC_NONE'];
            $arChange[] = array(
                "ID" => $arItem["ID"],
                "NAME" => $arItem["NAME"],
                "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
            );
            $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;*/
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_geocode.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}


function GetInfoByAddress($str) {
   $return = "";
   // ����������
   $params = array(
       'geocode' => $str, // �����
       'format' => 'json', // ������ ������
       'results' => 1, // ���������� ��������� �����������
       'lang' => 'ru-RU', // ���� ������
       'kind' => 'house'
   );
   $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
   if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
   {
      $coord = explode(" ", $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
      $return = $coord[1].",".$coord[0];
   }
   return $return;
}