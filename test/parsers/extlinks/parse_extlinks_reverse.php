<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 800,
    "PRINT" => true,
    "IBLOCK_ID" => 27
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 106817;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         //PrintObject($arItem);
         
         //��������� ���������
         $regReverse = '/(<noindex>(<a )((?!.*rel[ ]?=[ ]?[\"\\\']?nofollow[\"\\\']?)[^>]*>.*)<\/noindex>)/isU';
         
         //��������� ������ �����, ������� ���������� ��������
         $arParseFields = array();
         $arParseFields[] = "PREVIEW_TEXT";
         $arParseFields[] = "DETAIL_TEXT";
         $arParseFieldsChange = array();

         //������ ���� � �������� ����
         foreach($arParseFields as $parseField)
         {
            $parseText = $arItem[$parseField];
            if(strlen($parseText))
            {
               /*PrintObject($currentId);
               PrintObject($parseText);
               PrintObject(htmlentities($regReverse));*/
               $newText = preg_replace($regReverse, '$2rel="nofollow" $3', $parseText);
               /*PrintObject("-----------------------------------------------------");
               PrintObject($newText);*/
               if($newText !== NULL)
               {
                  if($parseText != $newText)
                  {
                     /*PrintObject($currentId);
                     PrintObject($parseText);
                     PrintObject("-----------------------------------------------------");
                     PrintObject($newText);*/
                     
                     $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                     //$arChange[] = $arItem["ID"];
                     $arChange[] = array(
                         "ID" => $arItem["ID"],
                         "NAME" => $arItem["NAME"],
                         "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                     );
                     $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
                     
                     $arParseFieldsChange[$parseField] = $newText;
                     $arParseFieldsChange[$parseField."_TYPE"] = "html";//$arItem[$parseField."_TYPE"];
                     if($KEY["PRINT"])
                     {
                        PrintObject(" ");
                        PrintObject(" ");
                        PrintObject("( ".$arItem["ID"]." )  ".$arItem["NAME"]."  ( ".$parseField." )");
                     }
                  }
               }
            }
         }
         //PrintObject($arParseFieldsChange);
         
         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      $currentKey = array_search($currentId, $arSessionId);
      if($currentKey !== false)
      {
         unset($arSessionId[$currentKey]);
         $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
      }
   }
   //�������������
   if(count($arSessionId) && !$KEY["MAXCOUNT"])
   {
      LocalRedirect("parse_extlinks_reverse.php");
      PrintObject("temp end");
   }
   else
   {
      PrintObject("END!!!");
   }
}