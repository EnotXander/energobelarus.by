<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 20,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 107032;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         //�����
         $arParseFieldsChange = array();
         //��������� ������ �����, ������� ���������� ��������
         $arParseFields = array();
         $arParseFields[] = "PREVIEW_TEXT";
         $arParseFields[] = "DETAIL_TEXT";
         //��������� ������ �������
         $arParseDomens = array();
         $arParseDomens[] = "energobelarus.by";
         $arParseDomens[] = "energobelarus.ru";
         $arParseDomens[] = "enb.by";
         //��������� ���������
         $reg = '/(<a[^>]*href=[\"\\\']?http[s]?:\/\/[^>\/\"\\\' ]*';
         foreach($arParseDomens as $domen)
            $reg .= "(?<!({$domen}))";
         $reg .= '[\/| |\"|\\\'][^>]*\>.*<\/a>)/isU';
         $regReverse = '/((<a )((?![^>]*rel[ ]?=[ ]?[\"\\\']?nofollow[\"\\\']?)[^>]*>))/isU';
         //���� ����������
         foreach($arParseFields as $parseField)
         {
            $parseText = $arItem[$parseField];
            $newText = $arItem[$parseField];
            if(strlen($newText))
            {
               //����� ������� ������
               preg_match_all($reg, $newText, $domen);
               foreach ($domen[1] as $el)
               {
                  PrintObject($el);
                  $newEl = preg_replace($regReverse, '$2rel="nofollow" $3', $el);
                  $newText = str_replace($el, $newEl, $newText);
               }
            }
            if($newText !== NULL)
            {
               if($parseText != $newText)
               {
                  /*PrintObject("-----------------");
                  PrintObject($parseText);
                  PrintObject("-----------------");
                  PrintObject($newText);
                  PrintObject("-----------------");*/
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  $arChange[] = array(
                      "ID" => $arItem["ID"],
                      "NAME" => $arItem["NAME"],
                      "FIELD" => $parseField,
                      "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                  );
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;

                  $arParseFieldsChange[$parseField] = $newText;
                  $arParseFieldsChange[$parseField."_TYPE"] = "html";//$arItem[$parseField."_TYPE"];
                  if($KEY["PRINT"])
                  {
                     PrintObject(" ");
                     PrintObject(" ");
                     PrintObject("( ".$arItem["ID"]." )  ".$arItem["NAME"]."  ( ".$parseField." )");
                  }
               }
            }
         }
         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_rel_nofollow.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}