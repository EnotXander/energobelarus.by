<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => false,
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 106854;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         $www = 0;
         $arParseFieldsChange = array();
         $newString = $arItem["TAGS"];
         //$newString = 'das d.s$a das, fsdfsd, gfghgfhgf, sf"fsdf\'fsdf, fff "ffdds", ttt22';
         $newString = explode(",", $newString);
         foreach ($newString as $wordKey => $word)
         {
            //PrintObject($word);
            $word = htmlspecialchars_decode($word);
            $word = preg_replace('/[\s	]+/', ' ', $word);
            $word = preg_replace('/[^a-z�-��-��-��-߸�0-9 \-\.�]+/i', '', $word);
            $newString[$wordKey] = trim($word);
            //PrintObject($word);
         }
         $newString = implode(", ", $newString);
         if($newString != $arItem["TAGS"])
            $arParseFieldsChange["TAGS"] = $newString;
         
         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               //PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
                 //    PrintObject("{$arItem["TAGS"]}   ---   {$newString}");
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  $arChange[] = $arItem["ID"];
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
                  if($KEY["PRINT"])
                  {
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
                     PrintObject("{$arItem["TAGS"]}");
                     PrintObject("{$newString}");
                     PrintObject(" --- ");
                  }
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_z2.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}