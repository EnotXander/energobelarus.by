<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 100,//��� ��������� 130
    "PRINT" => false,
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 118644;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         $arParseFieldsChange = array();
         if(!$arItem["DETAIL_PICTURE"])//���� ��������� �������� �� ������
         {
            if($arItem["PREVIEW_PICTURE"])//���������� �� ���������� ��������
            {
               $copyId = CFile::CopyFile($arItem["PREVIEW_PICTURE"]);
               $arFile = CFile::GetFileArray($copyId);
               $copyFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arFile["SRC"]);
               
               $arParseFieldsChange["DETAIL_PICTURE"] = $copyFile;
               $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
               $arChange[] = $arItem["ID"];
               $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
               if($KEY["PRINT"])
                  PrintObject ("���������� ��������! {$arItem["ID"]} ({$arItem["NAME"]})");
            }
            else
            {
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = $arItem["ID"];
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
               if($KEY["PRINT"])
                  PrintObject ("��� ��������! {$arItem["ID"]} ({$arItem["NAME"]})");
            }
         }
         if($arItem["PREVIEW_PICTURE"])
         {
            $arParseFieldsChange["PREVIEW_PICTURE"] = array('del' => 'Y');
         }
         
         //����������
         if($KEY["SAVE"])
         {
            if(is_array($arParseFieldsChange) && count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      $currentKey = array_search($currentId, $arSessionId);
      if($currentKey !== false)
      {
         unset($arSessionId[$currentKey]);
         $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
      }
   }
   //�������������
   if(count($arSessionId) && !$KEY["MAXCOUNT"])
   {
      LocalRedirect("parse_prevpic.php");
      PrintObject("temp end");
   }
   else
   {
      PrintObject("END!!!");
   }
}