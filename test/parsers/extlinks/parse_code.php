<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => false,
    "MAXCOUNT" => 1,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => false,
    "RELOAD" => false,
    "IBLOCK_ID" => 17
);

if($KEY["PARSE_AND_CHANGE"])
{
    //��������� ������� ��������������
      $arTransParams = array(
         "max_len" => 100,
         "change_case" => "L",
         "replace_space" => '_',
         "replace_other" => '_',
         "delete_repeat_replace" => true,
      );

   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 28530;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         //�����
         $arParseFieldsChange = array();
         //��������������
         $transName = CUtil::translit($arItem["NAME"], "ru", $arTransParams); //���������� ��������� ������� ��������������
         $arParseFieldsChange = Array("CODE" => $transName);
         
         $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
         $arChange[] = array(
             "ID" => $arItem["ID"],
             "NAME" => $arItem["NAME"],
             "CODE" => $transName,
             "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
         );
         $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;

         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_code.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}