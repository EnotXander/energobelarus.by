<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 5,//65 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => false,
    "IBLOCK_ID" => 17
);

$yaMetrika = YandexMetrika::getInstance(array(
   "ID" => METRIKA_CLIENT_ID,
   "SECRET" => METRIKA_CLIENT_SECRET,
   "USERNAME" => METRIKA_USERNAME,
   "PASSWORD" => METRIKA_PASSWORD
));

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 146936;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROP"] = $obItem->GetProperties();
         if(!$arItem["PROP"]["METRIKA_ID"]["VALUE"])
         {
            $yaMetrika->post("/counters", array(
                "name" => $arItem["CODE"],
                "site" => $_SERVER["SERVER_NAME"],
                "code_options" => array(
                    "clickmap" => 1,
                    "external_links" => 1,
                    "async" => 1,
                    "informer" => array(
                        "enabled" => 1,
                        "type" => "ext",
                        "allowed" => 1
                    ),
                    "denial" => 1
                )
            ));
            if(!strlen($yaMetrika->error))
            {
               if(!strlen($yaMetrika->result["errors"][0]["code"]))
               {
                  if($KEY["SAVE"])
                  {
                     $objElement->SetPropertyValues($arItem["ID"], $KEY["IBLOCK_ID"], $yaMetrika->result["counter"]["id"], "METRIKA_ID");
                     if($KEY["PRINT"])
                        PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) - {$yaMetrika->result["counter"]["id"]} ({$arItem["CODE"]})");
                  }
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  $arChange[] = array(
                     "ID" => $arItem["ID"],
                     "NAME" => $arItem["NAME"],
                     "METRIKA_ID" => $yaMetrika->result["counter"]["id"],
                     //"RES" => $yaMetrika->result,
                     "CODE" => $arItem["CODE"],
                     "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                  );
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
               }
               else
               {
                  $arErrors = array();
                  foreach($yaMetrika->result["errors"] as $err)
                     $arErrors[] = $err["code"];
                  $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
                  $arChange[] = array(
                     "ID" => $arItem["ID"],
                     "NAME" => $arItem["NAME"],
                     "ERROR" => $arErrors,
                     "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                  );
                  $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
               }
            }
            /*else
            {
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "ERROR" => $yaMetrika->error,
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
            }*/
         }
         /*else
         {
            $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
            $arChange[] = array(
               "ID" => $arItem["ID"],
               "NAME" => $arItem["NAME"],
               "METRIKA_ID" => $arItem["PROP"]["METRIKA_ID"]["VALUE"],
               "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
            );
            $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
         }*/
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_metrika.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}