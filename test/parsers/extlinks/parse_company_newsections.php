<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objSection = new CIBlockSection();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "MAXCOUNT" => 3000,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   //��������� ���� ��������
   $arBadSections = array();
   $res = $objSection->GetList(
           array("ID" => "DESC"),
           array(
               "IBLOCK_ID" => 17,
               "SECTION_ID" => 3079
            ),
           false
      );
   while($arItem = $res->GetNext())
      $arBadSections[$arItem["ID"]] = $arItem;
   //PrintAdmin($arBadSections);
   
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 29623;//*******
      
      $res = CIBlockElement::GetElementGroups($currentId);
      $arOriginGroups = array();
      $arEnabledGroups = array();
      while($arGroup = $res->GetNext())
      {
         $arOriginGroups[] = $arGroup["ID"];
         if(!in_array($arGroup["ID"], array_keys($arBadSections)))
         {
            $arEnabledGroups[] = $arGroup["ID"];
         }
      }
      //PrintAdmin($arEnabledGroups);
      if(count($arOriginGroups) != count($arEnabledGroups))
      {
         $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
         $arChange[] = array(
            "ID" => $currentId,
            "ORIGIN" => implode(" ", $arOriginGroups),
            "ENABLE" => implode(" ", $arEnabledGroups),
         );
         $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
         CIBlockElement::SetElementSection($currentId, $arEnabledGroups, false, 17);
      }
      
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_topsections.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}