<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => false,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
);

$TEST = array("WIDTH" => 450, "HEIGHT" => 450);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 57995;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         $arParseFieldsChange = array();
         $arParseFieldsChange[] = "PREVIEW_PICTURE";
         $arParseFieldsChange[] = "DETAIL_PICTURE";
         foreach ($arParseFieldsChange as $checkFieldKey => $checkFieldName)
         {
            if($arItem[$checkFieldName])
            {
               $arPicture = CFile::GetFileArray($arItem[$checkFieldName]);
               $match = true;
               foreach($TEST as $testName => $testValue)
               {
                  if($arPicture[$testName] != $testValue)
                  {
                     $match = false;
                     break;
                  }
               }
               if($match)
               {
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  if(isset($arChange[$arItem["ID"]]))
                  {
                     $arChange[$arItem["ID"]] = array(
                        "ID" => $arItem["ID"],
                        "NAME" => $arItem["NAME"],
                        "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"],
                        "FIELD" => "PREVIEW_PICTURE",
                        "FIELD2" => "DETAIL_PICTURE"
                     );
                  }
                  else
                  {
                     $arChange[$arItem["ID"]] = array(
                        "ID" => $arItem["ID"],
                        "NAME" => $arItem["NAME"],
                        "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"],
                        "FIELD" => $checkFieldName
                     );
                  }
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("http://".SITE_SERVER_NAME."/test/parsers/extlinks/parse_picture_nophoto.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}