<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//die();

if(!CModule::IncludeModule("iblock")) die();
$objSection = new CIBlockSection();
$objElement = new CIBlockElement();

$KEY = array(
    "IBLOCK_ID" => 3//3-news 2-articles 29-interviews 27-tovars 17-company 33-faces
);


$arSectionId = array();
$res = $objSection->GetList(
        array("ID" => "DESC"),
        array("IBLOCK_ID" => $KEY["IBLOCK_ID"]),
        false
   );
while($arItem = $res->GetNext())
{
   $arSectionId[] = (int)$arItem["ID"];
}

$res = $objElement->GetList(
        array("ID" => "DESC"),
        array("IBLOCK_ID" => $KEY["IBLOCK_ID"]),
        false
   );
while($arItem = $res->GetNext())
{
   if($arItem["IBLOCK_SECTION_ID"] && (array_search($arItem["IBLOCK_SECTION_ID"], $arSectionId) === false))
   {
      $arElementId[] = array(
          "ID" => $arItem["ID"],
          "NAME" => $arItem["NAME"],
          "ACTIVE_FROM" => $arItem["ACTIVE_FROM"],
          "IBLOCK_SECTION_ID" => $arItem["IBLOCK_SECTION_ID"],
          "DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"]
      );
   }

}
PrintAdmin($arElementId);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");