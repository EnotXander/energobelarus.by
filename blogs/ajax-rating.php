<?if($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
} else {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	LocalRedirect("/404.php", false, "404 Not found");
}?>

<?
if(CModule::IncludeModule("iblock")){
	$IBLOCK_ID = $_GET['iblockId'];
	$ELEMENT_ID = $_GET['elementId'];
	$RATING = $_GET['rating'];
	
	$resProperty = CIBlockElement::GetProperty(
		$IBLOCK_ID,
		$ELEMENT_ID,
		array("sort" => "asc"),
		array("CODE" => "RATING")
	);
	if($arrProperty = $resProperty->Fetch()){
		$propRating = IntVal($arrProperty["VALUE"]);
	}else{
		$propRating = 0;
	}
	
	if ($propRating){
		$setRating = ($propRating + $RATING) / 2;
		//��������� � ��������� �� 0,5
		$setRating = (round($setRating*2))/2;
	}
	
	$resUser = CUser::GetByID($USER->GetID());
	$arrUser = $resUser->Fetch();
	$arrIsRated = explode(',', $arrUser['UF_ISRATED']);
	
	//echo '<pre>'.print_r($arrIsRated,true).'</pre>';
	
	if (!in_array($ELEMENT_ID, $arrIsRated)){
		CIBlockElement::SetPropertyValuesEx(
			$ELEMENT_ID,
			$IBLOCK_ID,
			array(
				'RATING' => $_GET['rating']
			)
		);
		$curUser = new CUser;
		$arrIsRated[] = $ELEMENT_ID;
		$strIsRated = trim(implode(',', $arrIsRated), ',');
		$curUser->Update($USER->GetID(), array("UF_ISRATED" => $strIsRated));
	}
}
?>

<?if($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
} else {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
}?>