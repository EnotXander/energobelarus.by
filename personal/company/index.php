<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//���� ��������, � ������� �������������� ������� �
if(ENABLE_PREDSTAVITEL_MODE)
{

   $arPredstavitelInfo = PredstavitelGetByUser($USER->GetID());
   if((int)$arPredstavitelInfo["RELATED"] > 0)
   {
      $APPLICATION->SetTitle("���������� ���������");
      $sSectionName = "���������� ���������";
   }
    else
   {
       $APPLICATION->SetTitle("�������� ��������");
       $sSectionName = "�������� ��������";
   }
}
else
{
   if(!CModule::IncludeModule("iblock")) return;
   $rsCompany = CIBlockElement::GetList(
      array("NAME" => "ASC", "SORT" => "ASC"),
      array(
         "IBLOCK_ID" => 17,
         "ACTIVE" => "Y",
         "PROPERTY_USER" => $USER->GetID()
      )
   );
   if($arCompany = $rsCompany->GetNext())
   {
	   
	   
      $APPLICATION->SetTitle("���������� ���������");
      $sSectionName = "���������� ���������";
   }
    else
   {
       $APPLICATION->SetTitle("�������� ��������");
       $sSectionName = "�������� ��������";
   }
}
?>

<div class="middle_col">
    <h1><?$APPLICATION->ShowTitle();?></h1>
    <div class="clearfix"></div>
  <?$APPLICATION->IncludeComponent("whipstudio:iblock.element.add.form","", array(
        "SEF_MODE" => "N",
        "IBLOCK_TYPE" => "directories",
        "IBLOCK_ID" => 17,
        "PROPERTY_CODES" => array(
            //1 �������
            "������������ ����" => array(
                array(
                    "NAME",
                    GetPropertyId("ALTER_TITLE", 17),//ALTER_TITLE
                    305,
                    "IBLOCK_SECTION",
                    "DETAIL_TEXT",
                    123, // ������ 
                    84, // ������ 
                    94, // �����
                    99, // �����
                    102, // �����
                    100, // �������
                    70, // ��� email
                    308, // email ��� ��������
                 )
            ),
            //2 �������
            "�������������� ����" => array(
                array("DETAIL_PICTURE"),
                array(
                    195, // ������
                    317, // ����� ������
                    101, // ����� ������
                    69, // ���� ��������
                    GetPropertyId("SKYPE", 17), // �����
                    136 //�����-�����
                    //360 //����������� �����
                    //112, // �������� ����
                ),
                array(
                    GetPropertyId("RASCHETNY", 17), // ��������� ����
                    GetPropertyId("ADRESS_BANK", 17), // ����� �����
                    GetPropertyId("UNN", 17), // ���
                ),
                array(
                    GetPropertyId("ENGLISH_TITLE", 17), // ENGLISH_TITLE 
                    316, // ENGLISH_DESCRIPTION
                ),
                array(
                    292, // ����� � youtube
                )
           ),
           /*"����������" => array(
               array(
                    GetPropertyId("METRIKA_ID", 17), // �������
                )
           )*/
        ),
        "PROPERTY_CODES_REQUIRED" => array(
           "NAME",
           //305,
           "IBLOCK_SECTION",
           "DETAIL_TEXT",
           100, 
           308,
           99,
           123,
           84, 
           94, 
        ),
        "GROUPS" => array(5), // ������ ������� ����� ���������/�������������
        "STATUS_NEW" => 1,
        "STATUS" => array(1, 2, 3),
        "LIST_URL" => "",
        "ELEMENT_ASSOC" => "PROPERTY_ID", // �� ������ �������� ������ ����� � ��������������
        "ELEMENT_ASSOC_PROPERTY" => "USER",
        "MAX_USER_ENTRIES" => 10000,
        "MAX_LEVELS" => 100, // ���������� ������ � ���������
        "LEVEL_LAST" => "N",
        "USE_CAPTCHA" => "N",
        "USER_MESSAGE_EDIT" => "",
        "USER_MESSAGE_ADD" => "",
        "DEFAULT_INPUT_SIZE" => 30,
        "RESIZE_IMAGES" => "Y",
        "MAX_FILE_SIZE" => 0,
        "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
        "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
        "CUSTOM_TITLE_NAME" => "",
        "CUSTOM_TITLE_305" => "������������� ��������",
        "CUSTOM_TITLE_DETAIL_PICTURE" => "�������",
        "CUSTOM_TITLE_DETAIL_TEXT" => "���������� � ��������", 
        "CUSTOM_TITLE_IBLOCK_SECTION" => "����� ������������",
        "CUSTOM_TITLE_69" => "���� ��������",
        "CUSTOM_TITLE_70" => "Email ��� �����",
        "CUSTOM_TITLE_308" => "Email ��� ������",
        "CUSTOM_TITLE_102" => "��������� �� �����",    
        "CUSTOM_TITLE_316" => "Description in English",  
        "CUSTOM_TITLE_".GetPropertyId("ENGLISH_TITLE", 17) => "Title in english", 
        "CUSTOM_TITLE_".GetPropertyId("METRIKA_ID", 17) => "����������", 
        "SEF_FOLDER" => "/",
        "VARIABLE_ALIASES" => array()
     )
  );?>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
