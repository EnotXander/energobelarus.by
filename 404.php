<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $APPLICATION;
$APPLICATION->SetTitle("����� �������� �� ����������");
?>

<style>
   .content_box .left_col{
      width: auto !important;
      float: none !important;
   }
</style>

<div class="wrap404">
	<div class="main404">
		
		<div class="leftcol404 col404">
			<ul>
				<li><a href="/blogs/">�����</a></li>
				<li><a href="/school/">�����������</a></li>
				<li><a href="/reclame/">�������</a></li>
				<li><a href="/journals/">�������</a></li>
				<li><a href="/interview/">��������</a></li>
			</ul>
			<ul>
				<li><a href="/services/">������</a></li>
				<!-- <li><a href="/">��������</a></li> -->
				<!-- <li><a href="/">������</a></li> -->
				<li><a href="/events/">�����������</a></li>
				<li><a href="/company/">��������</a></li>
			</ul>
		</div>
		<div class="rightcol404 col404">
			<ul>
				<li><a href="/advert/">����������</a></li>
				<li><a href="/tender/">�������</a></li>
				<li><a href="/news/">�������</a></li>
				<!-- <li><a href="/">������</a></li> -->
				<li><a href="/terminology/">����������</a></li>
			</ul>
			<ul>
				<li><a href="/forum/">�����</a></li>
				<li><a href="/video/">�����</a></li>
				<li><a href="/photo/">����</a></li>
				<li><a href="/persons/">���������� � �����</a></li>
				<li><a href="/market/">������</a></li>
			</ul>
		</div>
		
		<div class="left404"></div>
		<div class="middle404">
			<div class="logo404">
				<img src="/bitrix/templates/energorb/images/logo404.png">
			</div>
			<div class="cont404">
				<h1>
					����� �������� �� ����������
				</h1>
				<p class="info404">
					404 not found
				</p>
				<a href="/" class="link404">
					��������� �� �������
				</a>
			</div>
		</div>
		<div class="right404"></div>
		<div class="bottom404"></div>
	</div>
	
	<div class="sections404">
		<div class="sections404-company sections404-inner">
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list","page404",array(
					"VIEW_MODE" => "TEXT",
					"SHOW_PARENT_NAME" => "Y",
					"IBLOCK_TYPE" => "directories",
					"IBLOCK_ID" => "17",
					"SECTION_ID" => '',
					"SECTION_CODE" => "",
					"SECTION_URL" => "",
					"COUNT_ELEMENTS" => "N",
					"TOP_DEPTH" => "1",
					"SECTION_FIELDS" => "",
					"SECTION_USER_FIELDS" => "",
					"ADD_SECTIONS_CHAIN" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_NOTES" => "",
					"CACHE_GROUPS" => "Y",
					'ORDERBY' => 'NAME ASC'
				)		
			);?>
		</div>
		<div class="sections404-market sections404-inner">
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list","page404",array(
					"VIEW_MODE" => "TEXT",
					"SHOW_PARENT_NAME" => "Y",
					"IBLOCK_TYPE" => "services",
					"IBLOCK_ID" => "27",
					"SECTION_ID" => '',
					"SECTION_CODE" => "",
					"SECTION_URL" => "",
					"COUNT_ELEMENTS" => "N",
					"TOP_DEPTH" => "1",
					"SECTION_FIELDS" => "",
					"SECTION_USER_FIELDS" => "",
					"ADD_SECTIONS_CHAIN" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_NOTES" => "",
					"CACHE_GROUPS" => "Y",
					'ORDERBY' => 'NAME ASC'
				)		
			);?>
		</div>
	</div>
	
</div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>