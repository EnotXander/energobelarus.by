<?

/**
 * Функция вывода объектов и массивов
 * @param $mas array входной массив или объект
 * @param $hidden bool режим невидимки
 */
function PrintObject($mas, $hidden = false) {
   $hiddenStyle = "";

   if ($hidden)
      $hiddenStyle = "display:none;";

   echo "<pre style=\"background-color:#FFF;color:#000;font-size:10px;" . $hiddenStyle . "\">";
   print_r($mas);
   echo "</pre>";
}

function PrintAdmin($obj)
{
    global  $USER;
    if($USER->IsAdmin())
    {
      echo "<pre style=\"background-color:#FFF;color:#000;font-size:10px;\"><span style=\"color: red;\">PrintAdmin:</span>\n";
      print_r($obj);
      echo "</pre>";
    }
}

/**
 * Функция масшатибрования изображений с поддержкой кеширования
 * Поддерживает разные режимы работы MODE
 * MODE принимает значения: cut, in, inv, width
 * @param str $req "/thumb/".$maxWidth."x".$maxHeight."xMODE".SRC
 */
function Resizer($req) {

   define('CHACHE_IMG_PATH', "{$_SERVER["DOCUMENT_ROOT"]}/images/cache/");
   define('RETURN_IMG_PATH', "/images/cache/");
   define("BX_FILE_PERMISSIONS", 0644);
   define("BX_DIR_PERMISSIONS", 0775);

   //создает каталоги если не существуют
   CheckDirPath(CHACHE_IMG_PATH);
   preg_match('/\/thumb\/([0-9]{1,4})x([0-9]{1,4})x([^\/]*)\/(.*)\.(gif|jpg|png|jpeg)/i', $req, $p);

   $path = "{$_SERVER["DOCUMENT_ROOT"]}/{$p[4]}.{$p[5]}";
   $temp = preg_replace("/\//", '_', $req);

   // если изображение существует
   if (is_file(CHACHE_IMG_PATH . $temp) == true) {
      return RETURN_IMG_PATH . $temp;
   }


   $i = getImageSize($path);

   if ($p[1] == 0)
      $p[1] = $p[2] / $i[1] * $i[0];
   if ($p[2] == 0)
      $p[2] = $p[1] / $i[0] * $i[1];



   if (($p[1] > $i[0] || $p[2] > $i[1]) && ($p[3] != "in" && $p[3] != "inv")) {
      $p[1] = $i[0];
      $p[2] = $i[1];
   }



   if ($p[3] == "prop") {
      $t[0] = $i[0]; // ширина
      $t[1] = $i[1]; // высота
      //PrintObject($p);
      //PrintObject($i);

      if ($i[0] > $p[1]) {
         $m = $p[1] / $i[0]; // множитель
         $i[0] = $p[1];
         $i[1] = $i[1] * $m;
      } else {
         
      }

      //PrintObject($i);

      if ($i[1] > $p[2]) {
         $m = $p[2] / $i[1]; // множитель
         $i[1] = $p[2];
         $i[0] = $p[1] * $m;
      } else {
         
      }

      $p[1] = $i[0];
      $p[2] = $i[1];

      //PrintObject($p);
      //PrintObject($t);
   }




   $im = ImageCreateTrueColor($p[1], $p[2]);
   imageAlphaBlending($im, false);
   switch (strtolower($p[5])) {
      case 'gif' :
         $i0 = ImageCreateFromGif($path);
         $icolor = imagecolorallocate($im, 255, 255, 255);
         imagefill($im, 0, 0, $icolor);
         break;
      case 'jpg' : case 'jpeg' :
         $i0 = ImageCreateFromJpeg($path);
         $icolor = imagecolorallocate($im, 255, 255, 255);
         imagefill($im, 0, 0, $icolor);
         break;
      case 'png' :
         $i0 = ImageCreateFromPng($path);
         $icolor = imagecolorallocate($im, 255, 255, 255);
         imagefill($im, 0, 0, $icolor);

         break;
   }

   switch (strtolower($p[3])) {
      case 'prop' :
         imageCopyResampled($im, $i0, 0, 0, 0, 0, $p[1], $p[2], $t[0], $t[1]);
         break;
      case 'cut' :
         $k_x = $i [0] / $p [1];
         $k_y = $i [1] / $p [2];
         if ($k_x > $k_y)
            $k = $k_y;
         else
            $k = $k_x;
         $pn [1] = $i [0] / $k;
         $pn [2] = $i [1] / $k;
         $x = ($p [1] - $pn [1]) / 2;
         $y = ($p [2] - $pn [2]) / 2;

         imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn[1], $pn[2], $i[0], $i[1]);

         break;
      case 'in' :

         if (($i [0] < $p [1]) && ($i [1] < $p [2])) {
            $k_x = 1;
            $k_y = 1;
         } else {
            $k_x = $i [0] / $p [1];
            $k_y = $i [1] / $p [2];
         }

         if ($k_x < $k_y)
            $k = $k_y;
         else
            $k = $k_x;

         $pn [1] = $i [0] / $k;
         $pn [2] = $i [1] / $k;

         $x = ($p [1] - $pn [1]) / 2;
         $y = ($p [2] - $pn [2]) / 2;
         imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn[1], $pn[2], $i[0], $i[1]);
         // 1 первый параметр изборажение источник
         // 2 изображение которое вставляется
         // 3 4 -х и у с какой точки будет вставятся в изображении источник
         // 5 6 - ширина и высота куда будет вписано изображение


         break;

      case 'inv' :
         $k_x = $i [0] / $p [1];
         $k_y = $i [1] / $p [2];
         if ($k_x < $k_y)
            $k = $k_y;
         else
            $k = $k_x;
         $pn [1] = $i [0] / $k;
         $pn [2] = $i [1] / $k;
         $x = ($p [1] - $pn [1]) / 2;
         $y = ($p [2] - $pn [2]) / 2;
         imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn[1], $pn[2], $i[0], $i[1]);

         break;

      case 'width' :
         $factor = $i[1] / $i[0]; // определяем пропорцию   height / width 

         if ($factor > 1.35) {
            $pn[1] = $p[1];
            $scale_factor = $i[0] / $pn[1]; // коэфффициент масштабирования
            $pn[2] = ceil($i[1] / $scale_factor);
            $x = 0;
            $y = 0;
            if (($p[2] / $pn[2]) < 0.6) {
               //echo 100 / ($pn[2] * 100) / ($p[2] *1.5);
               $pn[2] = (100 / (($pn[2] * 100) / ($p[2] * 1.3))) * $pn[2];
               $newKoef = $i[1] / $pn[2];
               $pn[1] = $i[0] / $newKoef;

               $x = ($p [1] - $pn [1]) / 2;
               //$y = ($p [2] - $pn [2]) / 2;
            }

            imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn[1], $pn[2], $i[0], $i[1]);
         } else {
            if (($i [0] < $p [1]) && ($i [1] < $p [2])) {
               $k_x = 1;
               $k_y = 1;
            } else {
               $k_x = $i [0] / $p [1];
               $k_y = $i [1] / $p [2];
            }

            if ($k_x < $k_y)
               $k = $k_y;
            else
               $k = $k_x;

            $pn [1] = $i [0] / $k;
            $pn [2] = $i [1] / $k;

            $x = ($p [1] - $pn [1]) / 2;
            $y = ($p [2] - $pn [2]) / 2;
            imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn[1], $pn[2], $i[0], $i[1]);
         }
         break;

      default : imageCopyResampled($im, $i0, 0, 0, 0, 0, $p[1], $p[2], $i[0], $i[1]);
         break;
   }

   if ($p[1] == 55 && $p[2] == 45 && $p[3] == "inv") {
      $i0 = ImageCreateFromPng($_SERVER["DOCUMENT_ROOT"] . "/img/video.png");
      imageCopyResampled($im, $i0, 0, 0, 0, 0, $p[1], $p[2], $p[1], $p[2]);
   }

   switch (strtolower($p[5])) {
      case 'gif' :imageSaveAlpha($im, true);
         @imageGif($im, CHACHE_IMG_PATH . $temp);
         break;
      case 'jpg' : case 'jpeg' : @imageJpeg($im, CHACHE_IMG_PATH . $temp, 100);
         break;
      case 'png' : imageSaveAlpha($im, true);
         @imagePng($im, CHACHE_IMG_PATH . $temp);
         break;
   }

   return RETURN_IMG_PATH . $temp;
}

/**
 * Функция вывода объектов и массивов
 * @param $val double входное значение в виде цифры с плавающей точкой
 */
function ParseTimeToString($val) {
   $dig = floor($val);
   $digAdd = $val - $dig;

   $string = '';

   if ($digAdd != 0) { // есть минуты

      $string .= (($dig > 9) ? $dig : "0" . $dig);
      $string = $string . ":";

      switch ($digAdd) {
         case 0.75: $string .= "45";
            break;
         case 0.5: $string .= "30";
            break;
         case 0.25: $string .= "15";
            break;
         default: $string .= "00";
      }
   } else { // нет минут
      $string .= (($dig > 9) ? $dig : "0" . $dig);
      $string = $string . ":00";
   }
   return $string;
}

function inFilter($type = 'full') {
   switch ($type) {
      case 160: return array(// бильярдные клубы
             236,// Обучение игре
             238,// Электронный дартс
             170, // Бар
             74, // Суши
             76, // Пицца
             88, // Разливное пиво
             92, // Коктейльная карта
             114, // TV с большим экраном
             116, // Спортивные трансляции
             132, // Wi-Fi
             134, // Кондиционер            
             136, // Охраняемая парковка
             138, // Обмен валют            
         );
         break;

      case 161: return array(// боулинг клубы
             236,// Обучение игре
             237,// Детский боулинг
             170,// Бар
             74, // Суши
             76, // Пицца
             88, // Разливное пиво
             92, // Коктейльная карта
             114, // TV с большим экраном
             116, // Спортивные трансляции
             132, // Wi-Fi
             134, // Кондиционер            
             136, // Охраняемая парковка
             138, // Обмен валют            
         );
         break;

      case 166: return array(// казино
             170, // Бар
             112, // Стриптиз
             114, // TV с большим экраном
             116, // Спортивные трансляции
             132, // Wi-Fi
             134, // Кондиционер            
             136, // Охраняемая парковка
             138, // Обмен валют            
         );
         break;

      case 167: return array(// караоке клубы
             74, // Суши
             76, // Пицца
             78, // Блюда на углях            
             80, // Меню завтраков
             82, // Обеденное меню
             84, // Кофейная карта
             86, // Чайная карта
             88, // Разливное пиво
             90, // Винная карта
             92, // Коктейльная карта            
             94, // Кальян
             98, // Танцпол        
             232, // Бэк-вокал
             233, // Ведущий
             234, // Запись выступления на диск
             114, // TV с большим экраном
             116, // Спортивные трансляции
             118, // VIP-зал
             130, // Банкетное обслуживание
             132, // Wi-Fi
             134, // Кондиционер
             136, // Охраняемая парковка
             138, // Обмен валют            
         );
         break;

      case 168: return array(// ночные клубы
             74, // Суши
             76, // Пицца
             84, // Кофейная карта
             86, // Чайная карта
             88, // Разливное пиво
             90, // Винная карта
             92, // Коктейльная карта 
             94, // Кальян
             168, // Бармен шоу
             112, // Стриптиз
             122, // Бильярд
             124, // Боулинг            
             114, // TV с большим экраном
             116, // Спортивные трансляции
             118, // VIP-зал
             130, // Банкетное обслуживание
             132, // Wi-Fi
             134, // Кондиционер
             136, // Охраняемая парковка
             138, // Обмен валют            
         );
         break;

      case 169: return array(// стрип клубы
             74, // Суши
             76, // Пицца
             84, // Кофейная карта
             86, // Чайная карта
             88, // Разливное пиво
             90, // Винная карта
             92, // Коктейльная карта 
             94, // Кальян  
             168, // Бармен шоу                    
             114, // TV с большим экраном
             116, // Спортивные трансляции
             118, // VIP-зал
             130, // Банкетное обслуживание
             132, // Wi-Fi
             134, // Кондиционер
             136, // Охраняемая парковка
             138, // Обмен валют            
         );
         break;

      default: return array(
             74, // Суши
             76, // Пицца
             88, // Разливное пиво
             80, // Меню завтраков
             82, // Обеденное меню
             94, // Кальян
             96, // Караоке
             102, // Дискотека
             106, // Вечеринки 
             100, // Живая музыка
             112, // Стриптиз
             114, // TV с большим экраном
             116, // Спортивные трансляции
             118, // VIP-зал
             120, // Детская комната
             122, // Бильярд
             124, // Боулинг
             126, // Дартс 
             128, // Выездное обслуживание
             130, // Банкетное обслуживание
             132, // Wi-Fi
             136, // Охраняемая парковка
             138, // Обмен валют
         );
   }
}

// возвращает список дополнительных свойств
function GetAdditionalProp($type = 'full') {
   switch ($type) {
      case 160: return array(// бильярдные клубы
             array(236),    // Обучение игре
             array(238),    // Элестронный дартс
             array(170), // Бар
             array(74), // Суши
             array(76), // Пицца
             array(88), // Разливное пиво
             array(92), // Коктейльная карта
             array(114), // TV с большим экраном 
             array(116), // Спортивные трансляции 
             array(132), // Wi-Fi
             array(134), // Кондиционер
             array(136), // Охраняемая парковка
             array(138), // Обмен валют              
         );
         break;

      case 161: return array(// боулинг клубы
             array(236),    // Обучение игре
             array(237),    // Детский боулинг
             array(170), // Бар
             array(74), // Суши
             array(76), // Пицца
             array(88), // Разливное пиво
             array(92), // Коктейльная карта
             array(114), // TV с большим экраном 
             array(116), // Спортивные трансляции 
             array(132), // Wi-Fi
             array(134), // Кондиционер
             array(136), // Охраняемая парковка
             array(138), // Обмен валют              
         );
         break;

      case 166: return array(// казино
             array(170), // Бар
             array(112), // Стриптиз
             array(114), // TV с большим экраном 
             array(116), // Спортивные трансляции 
             array(132), // Wi-Fi
             array(134), // Кондиционер
             array(136), // Охраняемая парковка
             array(138), // Обмен валют                       
         );
         break;

      case 167: return array(// караоке клубы
             array(74), // Суши
             array(76), // Пицца
             array(78), // Блюда на углях
             array(80), // Меню завтраков
             array(82), // Обеденное меню
             array(84), // Кофейная карта
             array(86), // Чайная карта
             array(88), // Разливное пиво
             array(90), // Винная карта
             array(92), // Коктейльная карта
             array(94), // Кальян
             array(98), // Танцпол
             array(232),    // Бэк-вокал
             array(233),    // Ведущий
             array(234),    // Запись выступления на диск
             array(114), // TV с большим экраном
             array(116), // Спортивные трансляции
             array(118), // VIP-зал
             array(130), // Банкетное обслуживание
             array(132), // Wi-Fi
             array(134), // Кондиционер
             array(136), // Охраняемая парковка
             array(138), // Обмен валют 
         );
         break;

      case 168: return array(// ночные клубы
             array(74), // Суши
             array(76), // Пицца
             array(84), // Кофейная карта
             array(86), // Чайная карта
             array(88), // Разливное пиво
             array(90), // Винная карта
             array(92), // Коктейльная карта
             array(94), // Кальян
             array(168), // Бармен шоу
             array(112), // Стриптиз
             array(122), // Бильярд
             array(124), // Боулинг
             array(114), // TV с большим экраном
             array(116), // Спортивные трансляции
             array(118), // VIP-зал
             array(130), // Банкетное обслуживание
             array(132), // Wi-Fi
             array(134), // Кондиционер
             array(136), // Охраняемая парковка
             array(138), // Обмен валют 
         );
         break;

      case 169: return array(// стрип клубы
             array(74), // Суши
             array(76), // Пицца
             array(84), // Кофейная карта
             array(86), // Чайная карта
             array(88), // Разливное пиво
             array(90), // Винная карта
             array(92), // Коктейльная карта
             array(94), // Кальян 
             array(168), // Бармен шоу
             array(114), // TV с большим экраном
             array(116), // Спортивные трансляции
             array(118), // VIP-зал
             array(130), // Банкетное обслуживание
             array(132), // Wi-Fi
             array(134), // Кондиционер
             array(136), // Охраняемая парковка
             array(138), // Обмен валют 
         );
         break;

      default: return array(
             array(74), // Суши -
             array(76), // Пицца -
             array(78), // Блюда на углях
             array(80), // Меню завтраков -
             array(82), // Обеденное меню -
             array(84), // Кофейная карта
             array(86), // Чайная карта
             array(88), // Разливное пиво -
             array(90), // Винная карта
             array(92), // Коктейльная карта
             array(94), // Кальян -
             array(96), // Караоке -
             array(98), // Танцпол
             array(232),    // Бэк-вокал
             array(233),    // Ведущий
             array(234),    // Запись выступления на диск
             array(236),    // Обучение игре
             array(237),    // Детский боулинг
             array(238),    // Элестронный дартс
             array(100), // Живая музыка -
             array(102), // Дискотека -
             array(104), // Шоу-программа
             array(106), // Вечеринки - 
             array(112), // Стриптиз -
             array(114), // TV с большим экраном - 
             array(116), // Спортивные трансляции - 
             array(118), // VIP-зал -
             array(120), // Детская комната -
             array(122), // Бильярд -
             array(124), // Боулинг -
             array(126), // Дартс - 
             array(128), // Выездное обслуживание -
             array(130), // Банкетное обслуживание -
             array(132), // Wi-Fi -
             array(134), // Кондиционер
             array(136), // Охраняемая парковка -
             array(138), // Обмен валют -  
         );
   }
}

// возвращает список свойств которые можно показывать
function GetCheckedProps(){
   return array(
       74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98,
       100, 102, 104, 106, 112, 114, 116, 118, 120, 122, 124,
       126, 128, 130, 132, 134, 136, 138, 168, 170, 199,
       21, 23, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48,
       164, 194, 192, 195, 193, 196, 197, 232, 233, 234, 236, 237, 238
   );
}
function GetShowCheck($part = "default", $isMobile = false) {
   $additional = array( 
       75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99,
       101, 103, 105, 107, 113, 115, 117, 119, 121, 123, 125,
       127, 129, 131, 133, 135, 137, 139, 169, 171, 200
   );

   $other = array(
       22, 24, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 165, 198
   );

   switch ($part) {
      case "additional": $result = $other;
         break;
      case "other": $result = $other;
         break;
      default: $result = array_merge($additional, $other);
         //array_merge
         break;
   }

   return $result;
}

// возвращает список свойств по которым можно фильтровать
function CanFilter($isMobile = false) {
   $result = GetAdditionalProp();

   foreach ($result as $key => $val)
      $result[$key] = $val[0];

   $addResult = array(
       28, // кухня
       38, // курение
       36, // станция метро
       34, // район
   );

   $result = array_merge($result, $addResult);

   return $result;
}

/**
 * Функция вывода объектов и массивов
 * @param $id int id свойтсва списка, которое необходимо достать
 * @param $result выходной массив содержащий id всех элементов списочного свойства
 * формат вывода $result["IDs"] - список ID-ков, и $result["INFO"] - список свойств со всеми данными
 */
function GetPropertyList($id) {
   $id = (int) $id;
   if ($id > 0) 
   {
      // подключаем кэш
      $obCache = new CPHPCache;
      $lifeTime = 60 * 60 * 24;
      $cacheID = md5("PROPERTY-LIST-{$id}");

      if ($obCache->InitCache($lifeTime, $cacheID, "/")) { // работаем с кэшем
         $tmp = $obCache->GetVars();
         $result = $tmp[$cacheID];
      } else {  // работаем с данными
         if (!CModule::IncludeModule("iblock"))
            return;
         $res = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), array("PROPERTY_ID" => $id));
         while ($temp = $res->Fetch()) {
            $result["IDs"][] = $temp["ID"];
            $result["INFO"][] = $temp;
         }

         $obCache->StartDataCache();
         $obCache->EndDataCache(array($cacheID => $result));
      }
      
      return $result;
   } else
      return false;
}

function GetPropertyListCode($code) {
   if (strlen($code) > 0) {
      if (!CModule::IncludeModule("iblock"))
         return;
      $res = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), array("PROPERTY_ID" => $code));
      while ($temp = $res->Fetch()) {
         $result["IDs"][] = $temp["ID"];
         $result["INFO"][] = $temp;
      }
      return $result;
   } else
      return false;
}

/* FirePHP */
/*require_once($_SERVER["DOCUMENT_ROOT"] . '/frameworks/FirePHPCore/fb.php');
global $firephp;
$firephp = FirePHP::getInstance(true);
FB::setEnabled(true);*/ //установить false для отключения вывода в консоль=======

/**
 * Функция склонения слов
 * @val $howmuch - int количество
 * @val $input - массив подстановок array ('комментариев', 'комментарий', 'комментария')
 * @return string;
 */
function Plural($howmuch, $input) {
   $howmuch = (int) $howmuch;
   $l2 = substr($howmuch, -2);
   $l1 = substr($howmuch, -1);
   if ($l2 > 10 && $l2 < 20)
      return $input[0];
   else
      switch ($l1) {
         case 0:
            return $input[0];
            break;
         case 1:
            return $input[1];
            break;
         case 2:
         case 3:
         case 4:
            return $input[2];
            break;
         default:
            return $input[0];
            break;
      }
   return false;
}

function ParseCurse() {
   // подключаем кэш
   $obCache = new CPHPCache;
   $lifeTime = 0/* 24*60*60 */;
   $cacheID = "CURSE-" . date("m.d.Y");

   if ($obCache->InitCache($lifeTime, $cacheID, "/")) { // работаем с кэшем
      $tmp = $obCache->GetVars();
      $arResult = $tmp[$cacheID];
   } else {  // работаем с данными
      require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');

      $ourCurse = array(145, 19, 190);
      $arResult = false;
      $objXML = new CDataXML();

      $content = file_get_contents("http://nbrb.by/Services/XmlExRates.aspx");

      if ($objXML->LoadString($content)) {
         $arr = $objXML->GetArray();
         foreach ($arr["DailyExRates"]['#']["Currency"] as $val) {
            if (!in_array($val["@"]["Id"], $ourCurse))
               continue;
            $arResult[$val["@"]["Id"]] = array(
                'NumCode' => $val['#']['NumCode'][0]['#'],
                'CharCode' => $val['#']['CharCode'][0]['#'],
                'Scale' => $val['#']['Scale'][0]['#'],
                'Name' => $val['#']['Name'][0]['#'],
                'Rate' => $val['#']['Rate'][0]['#'],
            );
         }
      }

      $obCache->StartDataCache();
      $obCache->EndDataCache(array($cacheID => $arResult));
   }

   return $arResult;
}

function ParseWhether() {
   // подключаем кэш
   $obCache = new CPHPCache;
   $lifeTime = 0/*60 * 60*/;
   $cacheID = "WHETHER-" . date("m/d/Y H");

   if ($obCache->InitCache($lifeTime, $cacheID, "/")) { // работаем с кэшем
      $tmp = $obCache->GetVars();
      $arResult = $tmp[$cacheID];
   } else {  // работаем с данными
      require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');

      $ourWhether = array('Minsk');
      $arResult = false;
      $objXML = new CDataXML();

      foreach ($ourWhether as $city) {
         $content = file_get_contents("http://www.google.com/ig/api?weather={$city}");

         if ($xml = new SimplexmlElement($content)) {
            foreach ($xml->weather as $item)
            {
               foreach ($item->current_conditions as $new)
                  $arResult = $new->temp_c['data'];
            }
         }
      }

      $obCache->StartDataCache();
      $obCache->EndDataCache(array($cacheID => $arResult));
   }

   return $arResult;
}

//Получение текущего URL
function request_url() {
   $result = ''; // Пока результат пуст
   $default_port = 80; // Порт по-умолчанию
   // А не в защищенном-ли мы соединении?
   if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) {
      // В защищенном! Добавим протокол...
      $result .= 'https://';
      // ...и переназначим значение порта по-умолчанию
      $default_port = 443;
   } else {
      // Обычное соединение, обычный протокол
      $result .= 'http://';
   }
   // �?мя сервера, напр. site.com или www.site.com
   $result .= $_SERVER['SERVER_NAME'];

   // А порт у нас по-умолчанию?
   if ($_SERVER['SERVER_PORT'] != $default_port) {
      // Если нет, то добавим порт в URL
      $result .= ':' . $_SERVER['SERVER_PORT'];
   }
   // Последняя часть запроса (путь и GET-параметры).
   $result .= $_SERVER['REQUEST_URI'];
   // Уфф, вроде получилось!
   return $result;
}



function GetDaysName($day) {
   $return = false;
   switch ($day) {
      case 0: $return = "Воскресенье";
         break;
      case 1: $return = "Понедельник";
         break;
      case 2: $return = "Вторник";
         break;
      case 3: $return = "Среда";
         break;
      case 4: $return = "Четверг";
         break;
      case 5: $return = "Пятница";
         break;
      case 6: $return = "Суббота";
         break;
   }
   return $return;
}



function GetMonthName($month) {
   $return = false;
   switch($month)
   {
      case 1:  $return = "Январь";
               break;
      case 2:  $return = "Февраль";
               break;
      case 3:  $return = "Март";
               break;
      case 4:  $return = "Апрель";
               break;
      case 5:  $return = "Май";
               break;
      case 6:  $return = "�?юнь";
               break;
      case 7:  $return = "�?юль";
               break;
      case 8:  $return = "Август";
               break;
      case 9:  $return = "Сентябрь";
               break;
      case 10: $return = "Октябрь";
               break;
      case 11: $return = "Ноябрь";
               break;
      case 12: $return = "Декабрь";
               break;
   }
   return $return;
}
function GetMonthNameGenitive($month) {
   $return = false;
   switch($month)
   {
      case 1:  $return = "Января";
               break;
      case 2:  $return = "Февраля";
               break;
      case 3:  $return = "Марта";
               break;
      case 4:  $return = "Апреля";
               break;
      case 5:  $return = "Мая";
               break;
      case 6:  $return = "�?юня";
               break;
      case 7:  $return = "�?юля";
               break;
      case 8:  $return = "Августа";
               break;
      case 9:  $return = "Сентября";
               break;
      case 10: $return = "Октября";
               break;
      case 11: $return = "Ноября";
               break;
      case 12: $return = "Декабря";
               break;
   }
   return $return;
}



//watermark
class CMyImageResizer 
{
   //static $www = "222";
   function ResizeImageGet($file, $arSize, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL, $bInitSizes = false, $arFilters = false, $arWaterMark = array())
   {
      if (!is_array($file) && IntVal($file) > 0)
      {
         $dbRes = CFile::GetByID(IntVal($file));
         $file = $dbRes->Fetch();
      }

      if (!is_array($file) || !array_key_exists("FILE_NAME", $file) || StrLen($file["FILE_NAME"]) <= 0)
         return false;

      if ($resizeType != BX_RESIZE_IMAGE_EXACT && $resizeType != BX_RESIZE_IMAGE_PROPORTIONAL_ALT)
         $resizeType != BX_RESIZE_IMAGE_PROPORTIONAL;

      if (!is_array($arSize))
         $arSize = array();
      if (!array_key_exists("width", $arSize) || IntVal($arSize["width"]) <= 0)
         $arSize["width"] = 0;
      if (!array_key_exists("height", $arSize) || IntVal($arSize["height"]) <= 0)
         $arSize["height"] = 0;
      $arSize["width"] = IntVal($arSize["width"]);
      $arSize["height"] = IntVal($arSize["height"]);

      $uploadDirName = COption::GetOptionString("main", "upload_dir", "upload");

      $imageFile = "/".$uploadDirName."/".$file["SUBDIR"]."/".$file["FILE_NAME"];

      $bNeedCreatePicture = ($arSize['width'] > $file['WIDTH'] && $arSize['height'] > $file['HEIGHT']) ? false : true;

      $cacheImageFile = "/".$uploadDirName."/resize_cache/".$file["SUBDIR"]."/".$arSize["width"]."_".$arSize["height"]."_".$resizeType.(is_array($arFilters)? md5(serialize($arFilters)): "").(is_array($arWaterMark)? md5(serialize($arWaterMark)): "")."/".$file["FILE_NAME"];

      $cacheImageFileCheck = $cacheImageFile;
      if ($file["CONTENT_TYPE"] == "image/bmp")
         $cacheImageFileCheck .= ".jpg";

      if (!file_exists($_SERVER["DOCUMENT_ROOT"].$cacheImageFileCheck))
      {
         /****************************** QUOTA ******************************/
         $bDiskQuota = true;
         if (COption::GetOptionInt("main", "disk_space") > 0)
         {
            $quota = new CDiskQuota();
            $bDiskQuota = $quota->checkDiskQuota($file);
         }
         /****************************** QUOTA ******************************/

         if ($bDiskQuota)
         {
            if(!is_array($arFilters))
               $arFilters = array(
                  array("name" => "sharpen", "precision" => 15),
               );
            $cacheImageFileTmp = $_SERVER["DOCUMENT_ROOT"].$cacheImageFile;

            if (CMyImageResizer::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$imageFile, $cacheImageFileTmp, $arSize, $resizeType, $arWaterMark, false, $arFilters))
            {
               $cacheImageFile = SubStr($cacheImageFileTmp, StrLen($_SERVER["DOCUMENT_ROOT"]));

               /****************************** QUOTA ******************************/
               if (COption::GetOptionInt("main", "disk_space") > 0)
                  CDiskQuota::updateDiskQuota("file", filesize($cacheImageFileTmp), "insert");
               /****************************** QUOTA ******************************/
            }
            else
            {
               $cacheImageFile = $imageFile;
            }
         }
         else
         {
            $cacheImageFile = $imageFile;
         }

         $cacheImageFileCheck = $cacheImageFile;
      }

      $arImageSize = array(0, 0);
      if ($bInitSizes)
         $arImageSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$cacheImageFileCheck);

      return array("src" => $cacheImageFileCheck, "width" => IntVal($arImageSize[0]), "height" => IntVal($arImageSize[1]));
   }
   
   function ResizeImageFile($sourceFile, &$destinationFile, $arSize, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL, $arWaterMark = array(), $jpgQuality=false, $arFilters=false)
   {
      static $bGD2 = false;
      static $bGD2Initial = false;

      if (!$bGD2Initial && function_exists("gd_info"))
      {
         $arGDInfo = gd_info();
         $bGD2 = ((StrPos($arGDInfo['GD Version'], "2.") !== false) ? true : false);
         $bGD2Initial = true;
      }

      $imageInput = false;
      $bNeedCreatePicture = false;
      $picture = false;

      if ($resizeType != BX_RESIZE_IMAGE_EXACT && $resizeType != BX_RESIZE_IMAGE_PROPORTIONAL_ALT)
         $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL;

      if (!is_array($arSize))
         $arSize = array();
      if (!array_key_exists("width", $arSize) || IntVal($arSize["width"]) <= 0)
         $arSize["width"] = 0;
      if (!array_key_exists("height", $arSize) || IntVal($arSize["height"]) <= 0)
         $arSize["height"] = 0;
      $arSize["width"] = IntVal($arSize["width"]);
      $arSize["height"] = IntVal($arSize["height"]);

      $arSourceSize = array("x" => 0, "y" => 0, "width" => 0, "height" => 0);
      $arDestinationSize = array("x" => 0, "y" => 0, "width" => 0, "height" => 0);

      $arSourceFileSizeTmp = getimagesize($sourceFile);
      if (!in_array($arSourceFileSizeTmp[2], array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_BMP)))
         return false;

      if (!file_exists($sourceFile) || !is_file($sourceFile))
         return false;

      if (CopyDirFiles($sourceFile, $destinationFile))
      {
         if (!is_array($arWaterMark))
            $arWaterMark = array();

         $sourceImage = false;
         switch ($arSourceFileSizeTmp[2])
         {
            case IMAGETYPE_GIF:
               $sourceImage = imagecreatefromgif($sourceFile);
               break;
            case IMAGETYPE_PNG:
               $sourceImage = imagecreatefrompng($sourceFile);
               break;
            case IMAGETYPE_BMP:
               $sourceImage = CFile::ImageCreateFromBMP($sourceFile);
               break;
            default:
               $sourceImage = imagecreatefromjpeg($sourceFile);
               break;
         }

         $sourceImageWidth = IntVal(imagesx($sourceImage));
         $sourceImageHeight = IntVal(imagesy($sourceImage));

         if ($sourceImageWidth > 0 && $sourceImageHeight > 0)
         {
            if ($arSize["width"] > 0 && $arSize["height"] > 0)
            {
               switch ($resizeType)
               {
                  case BX_RESIZE_IMAGE_EXACT:
                     $bNeedCreatePicture = true;
                     $width = Max($sourceImageWidth, $sourceImageHeight);
                     $height = Min($sourceImageWidth, $sourceImageHeight);

                     $iResizeCoeff = Max($arSize["width"] / $width, $arSize["height"] / $height);

                     $arDestinationSize["width"] = IntVal($arSize["width"]);
                     $arDestinationSize["height"] = IntVal($arSize["height"]);

                     if ($iResizeCoeff > 0)
                     {
                        $arSourceSize["x"] = ((($sourceImageWidth * $iResizeCoeff - $arSize["width"]) / 2) / $iResizeCoeff);
                        $arSourceSize["y"] = ((($sourceImageHeight * $iResizeCoeff - $arSize["height"]) / 2) / $iResizeCoeff);
                        $arSourceSize["width"] = $arSize["width"] / $iResizeCoeff;
                        $arSourceSize["height"] = $arSize["height"] / $iResizeCoeff;
                     }

                     break;
                  default:
                     if ($resizeType == BX_RESIZE_IMAGE_PROPORTIONAL_ALT)
                     {
                        $width = Max($sourceImageWidth, $sourceImageHeight);
                        $height = Min($sourceImageWidth, $sourceImageHeight);
                     }
                     else
                     {
                        $width = $sourceImageWidth;
                        $height = $sourceImageHeight;
                     }
                     $ResizeCoeff["width"] = $arSize["width"] / $width;
                     $ResizeCoeff["height"] = $arSize["height"] / $height;

                     $iResizeCoeff = Min($ResizeCoeff["width"], $ResizeCoeff["height"]);
                     $iResizeCoeff = ((0 < $iResizeCoeff) && ($iResizeCoeff < 1) ? $iResizeCoeff : 1);
                     $bNeedCreatePicture = ($iResizeCoeff != 1 ? true : false);

                     $arDestinationSize["width"] = intVal($iResizeCoeff * $sourceImageWidth);
                     $arDestinationSize["height"] = intVal($iResizeCoeff * $sourceImageHeight);

                     $arSourceSize["x"] = 0;
                     $arSourceSize["y"] = 0;
                     $arSourceSize["width"] = $sourceImageWidth;
                     $arSourceSize["height"] = $sourceImageHeight;
                     break;
               }
            }
            else
            {
               $arSourceSize = array("x" => 0, "y" => 0, "width" => $sourceImageWidth, "height" => $sourceImageHeight);
               $arDestinationSize = array("x" => 0, "y" => 0, "width" => $sourceImageWidth, "height" => $sourceImageHeight);

               $arSize["width"] = $sourceImageWidth;
               $arSize["height"] = $sourceImageHeight;
            }

            $bNeedCreatePicture = (!empty($arWaterMark['path_to_watermark']) ? true : $bNeedCreatePicture);

            if ($bNeedCreatePicture)
            {
               if ($bGD2)
               {
                  $picture = ImageCreateTrueColor($arDestinationSize["width"], $arDestinationSize["height"]);
                  if($arSourceFileSizeTmp[2] == IMAGETYPE_PNG)
                  {
                     $transparentcolor = imagecolorallocatealpha($picture, 0, 0, 0, 127);
                     imagefilledrectangle($picture, 0, 0, $arDestinationSize["width"], $arDestinationSize["height"], $transparentcolor);
                     $transparentcolor = imagecolortransparent($picture, $transparentcolor);


                     imagealphablending($picture, false);
                     imagecopyresampled($picture, $sourceImage,
                        0, 0, $arSourceSize["x"], $arSourceSize["y"],
                        $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
                     imagealphablending($picture, true);
                  }
                  elseif($arSourceFileSizeTmp[2] == IMAGETYPE_GIF)
                  {
                     imagepalettecopy($picture, $sourceImage);

                     //Save transparency for GIFs
                     $transparentcolor = imagecolortransparent($sourceImage);
                     if($transparentcolor >= 0 && $transparentcolor < imagecolorstotal($sourceImage))
                     {
                        $transparentcolor = imagecolortransparent($picture, $transparentcolor);
                        imagefilledrectangle($picture, 0, 0, $arDestinationSize["width"], $arDestinationSize["height"], $transparentcolor);
                     }

                     imagecopyresampled($picture, $sourceImage,
                        0, 0, $arSourceSize["x"], $arSourceSize["y"],
                        $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
                  }
                  else
                  {
                     imagecopyresampled($picture, $sourceImage,
                        0, 0, $arSourceSize["x"], $arSourceSize["y"],
                        $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
                  }
               }
               else
               {
                  $picture = ImageCreate($arDestinationSize["width"], $arDestinationSize["height"]);
                  imagecopyresized($picture, $sourceImage,
                     0, 0, $arSourceSize["x"], $arSourceSize["y"],
                     $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
               }
            }

            if(is_array($arFilters))
            {
               foreach($arFilters as $arFilter)
                  CFile::ApplyImageFilter($picture, $arFilter);
            }
            
            if (!empty($arWaterMark['path_to_watermark']) && file_exists($_SERVER['DOCUMENT_ROOT'].$arWaterMark['path_to_watermark'])) {
               $pngWaterMarkImg = @imagecreatefrompng($_SERVER['DOCUMENT_ROOT'].$arWaterMark['path_to_watermark']);
               
               $arWaterMarkImgSize['x'] = imagesx($pngWaterMarkImg);
               $arWaterMarkImgSize['y'] = imagesy($pngWaterMarkImg);
               $arWaterMarkImgSize['width'] = $arWaterMarkImgSize['x'];
               $arWaterMarkImgSize['height'] = $arWaterMarkImgSize['y'];
               if(($arDestinationSize['width'] < $arWaterMarkImgSize['x']) || ($arDestinationSize['height'] < $arWaterMarkImgSize['y']))//если картинка меньше ватермарка
               {
                  
                  $maxKoef = max(array($arWaterMarkImgSize['x']/$arDestinationSize['width'], $arWaterMarkImgSize['y']/$arDestinationSize['height']));
                  //задаем новые размеры ватермарка
                  $arWaterMarkImgSize['x'] = intval($arWaterMarkImgSize['x']/$maxKoef);
                  $arWaterMarkImgSize['y'] = intval($arWaterMarkImgSize['y']/$maxKoef);
               }
               if($arDestinationSize['width']/$arWaterMarkImgSize['x'] > 2)//если в 2 раза больше
                  $arWaterMarkDestPos['x'] = intval($arDestinationSize['width'] - $arWaterMarkImgSize['x']) - intval($arWaterMark['margin_x']);
               else
                  $arWaterMarkDestPos['x'] = intval(($arDestinationSize['width']-$arWaterMarkImgSize['x'])/2);
               $arWaterMarkDestPos['y'] = intval($arDestinationSize['height'] - $arWaterMarkImgSize['y'] - intval($arWaterMark['margin_y']));
               
               imagecopyresampled($picture, $pngWaterMarkImg, $arWaterMarkDestPos['x'], $arWaterMarkDestPos['y'] - intval($arWaterMark['margin_y']), 0, 0, $arWaterMarkImgSize['x'], $arWaterMarkImgSize['y'], $arWaterMarkImgSize['width'], $arWaterMarkImgSize['height']);
               imagedestroy($pngWaterMarkImg);
            }

            if ($bNeedCreatePicture)
            {
               if(file_exists($destinationFile))
                  unlink($destinationFile);
               switch ($arSourceFileSizeTmp[2])
               {
                  case IMAGETYPE_GIF:
                     imagegif($picture, $destinationFile);
                     break;
                  case IMAGETYPE_PNG:
                     imagealphablending($picture, false );
                     imagesavealpha($picture, true);
                     imagepng($picture, $destinationFile);
                     break;
                  default:
                     if ($arSourceFileSizeTmp[2] == IMAGETYPE_BMP)
                        $destinationFile .= ".jpg";
                     if($jpgQuality === false)
                        $jpgQuality = intval(COption::GetOptionString('main', 'image_resize_quality', '95'));
                     if($jpgQuality <= 0 || $jpgQuality > 100)
                        $jpgQuality = 95;
                     imagejpeg($picture, $destinationFile, $jpgQuality);
                     break;
               }
               imagedestroy($picture);
            }
         }

         return true;
      }

      return false;
   }
}

function createBase64Img($path) {   
   $extension = getExtension($path);
   $file = $_SERVER["DOCUMENT_ROOT"].$path;
   if($fp = fopen($file,"rb", 0))
   {
      $picture = fread($fp, filesize($file));
      fclose($fp);
      // base64 encode the binary data, then break it
      // into chunks according to RFC 2045 semantics
      $base64 = chunk_split(base64_encode($picture));
      
      $src = $base64;      
      return $src;
   }
}

function getExtension($filename) {
   $path_info = pathinfo($filename);
   return $path_info['extension'];
}

  
function toJSON($o) {
	switch (gettype($o)) {
		case 'NULL':
			return 'null';
		case 'integer':
		case 'double':
			return strval($o);
		case 'string':
			return '"' . addslashes($o) . '"';
		case 'boolean':
			return $o ? 'true' : 'false';
		case 'object':
			$o = (array) $o;
		case 'array':
			$foundKeys = false;

			foreach ($o as $k => $v) {
				if (!is_numeric($k)) {
					$foundKeys = true;
					break;
				}
			}

			$result = array();

			if ($foundKeys) {
				foreach ($o as $k => $v) {
					$result []= toJSON($k) . ':' . toJSON($v);
				}

				return '{' . implode(',', $result) . '}';
			} else {
				foreach ($o as $k => $v) {
					$result []= toJSON($v);
				}

				return '[' . implode(',', $result) . ']';
			}
	}
}


function getRaiting($id) {
   require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');
   $objXML = new CDataXML();
   $content = file_get_contents("http://www.kinopoisk.ru/rating/{$id}.xml");

   $result = false;
   if ($objXML->LoadString($content)) 
   {
      $xml = $objXML->GetArray();
      $result["KP"]["COUNT"] = $xml["rating"]["#"]["kp_rating"][0]["@"]["num_vote"];
      $result["KP"]["VALUE"] = $xml["rating"]["#"]["kp_rating"][0]["#"];
      
      $result["IMDB"]["COUNT"] = $xml["rating"]["#"]["imdb_rating"][0]["@"]["num_vote"];
      $result["IMDB"]["VALUE"] = $xml["rating"]["#"]["imdb_rating"][0]["#"];
   }   
   return $result;
}

function getMonth($month) {
   $month = strtolower(trim($month));
   $return = false;
   switch($month)
   {
      case "января":       $return = 1;
                                       break;
      case "февраля":    $return = 2;
                                       break;
      case "марта":        $return = 3;
                                       break;
      case "апреля":       $return = 4;
                                       break;
      case "мая":             $return = 5;
                                       break;
      case "июня":           $return = 6;
                                       break;
      case "июля":           $return = 7;
                                       break;
      case "августа":        $return = 8;
                                       break;
      case "сентября":     $return = 9;
                                       break;
      case "октября":      $return = 10;
                                       break;
      case "ноября":       $return = 11;
                                       break;
      case "декабря":      $return = 12;
                                       break;
   }
   return $return;
}


function GetInfoByCoord($coord_e, $coord_n) {
   $return = false;

   if(strlen($coord_e) && strlen($coord_n))
   {
      //$obCache = new CPHPCache;
      //$lifeTime = 60 * 60 * 24; // 1 день
      //$cacheID = md5("USER-COORD-" . $coord_e . "-" . $coord_n);
      
      /*if ($obCache->InitCache($lifeTime, $cacheID, '/') || $_GET["clear_cache"] == "Y") // работаем с кэшем
      {
         $tmp = $obCache->GetVars();
         $return = $tmp[$CACHE_ID];
      } else
      {*/
         $return = array();

         // ДОМ
         $params = array(
             'geocode' => $coord_e . "," . $coord_n, // координаты
             'format' => 'json', // формат ответа
             'results' => 1, // количество выводимых результатов
             'lang' => 'ru-RU', // язык ответа
             'kind' => 'house'
         );

         $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
         if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
         {
            // с административным делением
            if(isset($response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea))
            {
               $return["HOUSE"] = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->Locality->Thoroughfare->ThoroughfareName;
            
               if(strlen($response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->Locality->Thoroughfare->Premise->PremiseNumber) > 0)
                  $return["HOUSE"] .= ", ". $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->Locality->Thoroughfare->Premise->PremiseNumber;
               $return["CITY_NAME"] = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->Locality->LocalityName;
            } else // без административного деления
            {
               $return["HOUSE"] = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->Locality->Thoroughfare->ThoroughfareName;
            
               if(strlen($response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->Locality->Thoroughfare->Premise->PremiseNumber) > 0)
                  $return["HOUSE"] .= ", ". $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->Locality->Thoroughfare->Premise->PremiseNumber;
               $return["CITY_NAME"] = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->Locality->LocalityName;
            }
         }
         
         // МЕТРО
         $params = array(
             'geocode' => $coord_e . "," . $coord_n, // координаты
             'format' => 'json', // формат ответа
             'results' => 1, // количество выводимых результатов
             'lang' => 'ru-RU', // язык ответа
             'kind' => 'metro'
         );

         $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
         if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
            $return["METRO"] = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->Locality->Thoroughfare->Premise->PremiseName;

         // РАЙОН
         $params = array(
             'geocode' => $coord_e . "," . $coord_n, // координаты
             'format' => 'json', // формат ответа
             'results' => 1, // количество выводимых результатов
             'lang' => 'ru-RU', // язык ответа
             'kind' => 'district'
         );

         $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
         if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
         {
            if(isset($response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea))
               $return["DISTRICT"] = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->Locality->DependentLocality->DependentLocalityName;
            else
               $return["DISTRICT"] = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->Locality->DependentLocality->DependentLocalityName;
         }
         
         //$obCache->StartDataCache();
         //$obCache->EndDataCache(array($CACHE_ID => $return));
      //}
   }
   
   //SetCity($return["CITY_NAME"]);
   
   return $return;
}

//������� SuperPrint///
echo '
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$("ul").hide();
	$("ul li:odd").css();
	$(".click_span").click(function(){
		$(this).next().slideToggle();
	});
});
</script>


<style type="text/css">
.click_span{
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	padding: 1px;
	background: -moz-linear-gradient(center top, #efefef 0%,#e0e0e0 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #efefef),color-stop(1, #e0e0e0));
	border: 1px solid #d1d1d1;
	font-size: 16px;
	text-shadow: 1px 1px 0px #fff;
	margin:0;
	width:15px;
	text-align:center;
	}
.click_span {
	float: left;
	cursor: pointer;
	}
.click_span:hover {
	text-shadow: 0px 0px 3px #a1eeff;
	}

ul {
	padding: 5px;
	overflow: hidden;
	margin:0;
	}
ul li {
	font-size: 13px;
	list-style-type: square;
	list-style-position: inside;
	padding: 5px;
	}
	</style>
';

function superPrint($array)
{
	$i=0;
	echo "<div class='click_span' style='width:20px;'>+</div><ul class='active'>(";
	echo "<div style='margin:0px 0px 0px 30px;'>";

	foreach($array as $key => $arr)
	{
		echo '<li style="list-style-type:none;">['.$key.']';
		echo '&nbsp=>&nbsp';
		if (is_array($arr))
		{
			Superprint($arr);
		}
		else
		{
			echo $arr.'</li>';
		}
	$i++;	
	}
echo '</div>)</ul>';
}
